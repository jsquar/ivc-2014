// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
#include "KapselLuke.pov"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 15      // front view
                            location  <0.0 , 1.0 ,-40.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera { angle 14   // diagonal view
                            location  <20.0 , 15.0 ,-20.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1 , 0.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 3.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}      
                            
camera{Camera_0}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>*0.6} 
          
           } //end of skysphere
//------------------------------ the Axes --------------------------------
//------------------------------------------------------------------------
#macro Axis_( AxisLen, Dark_Texture,Light_Texture) 
 union{
    cylinder { <0,-AxisLen,0>,<0,AxisLen,0>,0.05
               texture{checker texture{Dark_Texture } 
                               texture{Light_Texture}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{Dark_Texture}
         }
     } // end of union                   
#end // of macro "Axis()"
//------------------------------------------------------------------------
#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ, Tex_Dark, Tex_Light)
//--------------------- drawing of 3 Axes --------------------------------
union{
#if (AxisLenX != 0)
 object { Axis_(AxisLenX, Tex_Dark, Tex_Light)   rotate< 0,0,-90>}// x-Axis
 text   { ttf "arial.ttf",  "x",  0.15,  0  texture{Tex_Dark} 
          rotate<20,-45,0> scale 0.75 translate <AxisLenX+0.05,0.4,-0.10> no_shadow}
#end // of #if 
#if (AxisLenY != 0)
 object { Axis_(AxisLenY, Tex_Dark, Tex_Light)   rotate< 0,0,  0>}// y-Axis
 text   { ttf "arial.ttf",  "y",  0.15,  0  texture{Tex_Dark}    
          rotate<10,0,0> scale 0.75 translate <-0.65,AxisLenY+0.50,-0.10>  rotate<0,-45,0> no_shadow}
#end // of #if 
#if (AxisLenZ != 0)
 object { Axis_(AxisLenZ, Tex_Dark, Tex_Light)   rotate<90,0,  0>}// z-Axis
 text   { ttf "arial.ttf",  "z",  0.15,  0  texture{Tex_Dark}
          rotate<20,-45,0> scale 0.85 translate <-0.75,0.2,AxisLenZ+0.10> no_shadow}
#end // of #if 
} // end of union
#end// of macro "AxisXYZ( ... )"
//------------------------------------------------------------------------

#declare Texture_A_Dark  = texture {
                               pigment{ color rgb<1,0.45,0>}
                               finish { phong 1}
                             }
#declare Texture_A_Light = texture { 
                               pigment{ color rgb<1,1,1>}
                               finish { phong 1}
                             }

//object{ AxisXYZ( 4.50, 3.00, 5.00, Texture_A_Dark, Texture_A_Light)}
//-------------------------------------------------- end of coordinate axes


// ground -----------------------------------------------------------------
//---------------------------------<<< settings of squared plane dimensions
#declare RasterScale = 1.0;
#declare RasterHalfLine  = 0.035;  
#declare RasterHalfLineZ = 0.035; 
//-------------------------------------------------------------------------
#macro Raster(RScale, HLine) 
       pigment{ gradient x scale RScale
                color_map{[0.000   color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,0>*0.6]
                          [1.000   color rgbt<1,1,1,0>*0.6]} }
 #end// of Raster(RScale, HLine)-macro    
//-------------------------------------------------------------------------
    

plane { <0,1,0>, 0    // plane with layered textures
        texture { pigment{color White*1.1}
                  finish {ambient 0.45 diffuse 0.85}}
        texture { Raster(RasterScale,RasterHalfLine ) rotate<0,0,0> }
        texture { Raster(RasterScale,RasterHalfLineZ) rotate<0,90,0>}
        rotate<0,0,0>
      }
//------------------------------------------------ end of squared plane XZ   */
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
#declare KapselHoehe = 3.5;

#declare RumpfTextur =
    material{
  texture{
    pigment{rgb <1,1,1>}
    finish{
      conserve_energy
      diffuse 0.6
      ambient 0
      specular 0.2
      roughness 0.05
      reflection{0 1 fresnel on metallic 0}
    }
  }
  interior{ior 1.16}
}           

#local AchtungBandTextur =
              texture{ pigment{ gradient <1,1,0>
                            color_map{
                                [ 0.0 color rgb<1,1,1> ]
                                [ 0.3 color rgb<1,1,1> ]
                                [ 0.3 color rgb<1,0,0>*0.3 ]
                                [ 1.0 color rgb<1,0,0>*0.7 ]
                               } // end color_map
                             scale 0.3
                           } // end pigment
                 //normal  { bumps 0.5 scale  0.005 }
                   finish  { phong 1 reflection 0.00 }
                 } // end of texture -------------------


#declare Kapsel =
    union
    {      
        difference
        {
            cone { <0,0,0>,1.5,<0,2.5,0>,0.5 
                  material { RumpfTextur } // end of texture
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------
            cone { <0,0.1,0>,1.4,<0,2.40,0>,0.4 
                  texture { pigment{ color rgb<1,0.60,0.0>}
                    finish { phong 1 reflection{ 0.00 metallic 0.00} }} // end of texture
                     scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------   
            
            box { <0,0,0>,< 2.00, 1.20, 1.00>   
                material {RumpfTextur}              
                scale <1,1,1> rotate<0,0,0> translate<0,0.4,-0.5> 
                } // end of box --------------------------------------

         }         //Ende von Difference     
             
         //AchtungBand
         /*
         difference
         {             
            cylinder { <0,0,0>,<0,.10,0>, 1.50 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
            cylinder { <0,-0.1,0>,<0,.20,0>, 1.40 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
         }//Ende von Difference
                               */             
         //Luke
         object { KapselLuke rotate<0,-90,25> translate<1.15,1.1,0> }   
         
         //Konstruktion der Raketenspitze
                        
         //Verbindung                        
         cone { <0,2.5,0>,0.5,<0,2.6,0>,0.46 
                         texture { pigment{ color rgbf <.4,0.4,0.4,0> } 
                     normal { bumps 0.5 scale 0.25}
                     finish { phong 0.2 reflection 0.30}} 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------

         //Orangenschale
         cone { <0,2.6,0>,0.46,<0,3.4,0>,0.1 
                           texture { spiral1 5 scale 0.5                   //---------------- 
                   texture_map{ [0.25 pigment{ color rgbf <.4,0.4,0.4,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}]
                                [0.25 pigment{ color rgbf <1,1,1,1>} ]
                                [0.75 pigment{ color rgbf <1,1,1,1>} ]
                                [0.75 pigment{ color rgbf <.4,0.4,0.4,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}]
                              } // end of texture_map
                    rotate<90,0,0>
                 } // end of texture ------------------------------------- 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------           
            
            //Unterliegendes Metall
            cone { <0,2.6,0>,0.45,<0,3.4,0>,0.09 
                         texture{ pigment{ color rgbf <1,0.65,0.2,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}}
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone ------------------------------------- 
                      
            //Spitze          
            cone { <0,3.4,0>,0.1,<0,3.5,0>,0.04 
                         texture { pigment {color rgbf <.4,0.4,0.4,0>} 
                     normal { bumps 0.5 scale 0.25}
                     finish { phong 0.2 reflection 0.30}} 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------
            
        translate <0,-KapselHoehe/2,0>

    }//Ende von Union
    
   
//object{Kapsel}