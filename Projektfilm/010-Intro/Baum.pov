// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
//------------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 15      // front view
                            location  <0.0 , 1.0 ,-40.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera { angle 14   // diagonal view
                            location  <20.0 , 15.0 ,-20.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1 , 0.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera {angle 90        // top view
                            location  <0.0 , 3.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_0}
//------------------------------------------------------------------------
// sun -------------------------------------------------------------------
light_source{<1500,2500,-2500> color White}
// sky -------------------------------------------------------------------
sky_sphere{ pigment{ gradient <0,1,0>
                     color_map{ [0   color rgb<1,1,1>         ]//White
                                [0.4 color rgb<0.14,0.14,0.56>]//~Navy
                                [0.6 color rgb<0.14,0.14,0.56>]//~Navy
                                [1.0 color rgb<1,1,1>         ]//White
                              }
                     scale 2 }
           } // end of sky_sphere 
//------------------------------------------------------------------------

//------------------------------ the Axes --------------------------------
//------------------------------------------------------------------------
#macro Axis_( AxisLen, Dark_Texture,Light_Texture) 
 union{
    cylinder { <0,-AxisLen,0>,<0,AxisLen,0>,0.05
               texture{checker texture{Dark_Texture } 
                               texture{Light_Texture}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{Dark_Texture}
         }
     } // end of union                   
#end // of macro "Axis()"
//------------------------------------------------------------------------
#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ, Tex_Dark, Tex_Light)
//--------------------- drawing of 3 Axes --------------------------------
union{
#if (AxisLenX != 0)
 object { Axis_(AxisLenX, Tex_Dark, Tex_Light)   rotate< 0,0,-90>}// x-Axis
 text   { ttf "arial.ttf",  "x",  0.15,  0  texture{Tex_Dark} 
          rotate<20,-45,0> scale 0.75 translate <AxisLenX+0.05,0.4,-0.10> no_shadow}
#end // of #if 
#if (AxisLenY != 0)
 object { Axis_(AxisLenY, Tex_Dark, Tex_Light)   rotate< 0,0,  0>}// y-Axis
 text   { ttf "arial.ttf",  "y",  0.15,  0  texture{Tex_Dark}    
          rotate<10,0,0> scale 0.75 translate <-0.65,AxisLenY+0.50,-0.10>  rotate<0,-45,0> no_shadow}
#end // of #if 
#if (AxisLenZ != 0)
 object { Axis_(AxisLenZ, Tex_Dark, Tex_Light)   rotate<90,0,  0>}// z-Axis
 text   { ttf "arial.ttf",  "z",  0.15,  0  texture{Tex_Dark}
          rotate<20,-45,0> scale 0.85 translate <-0.75,0.2,AxisLenZ+0.10> no_shadow}
#end // of #if 
} // end of union
#end// of macro "AxisXYZ( ... )"
//------------------------------------------------------------------------

#declare Texture_A_Dark  = texture {
                               pigment{ color rgb<1,0.45,0>}
                               finish { phong 1}
                             }
#declare Texture_A_Light = texture { 
                               pigment{ color rgb<1,1,1>}
                               finish { phong 1}
                             }

object{ AxisXYZ( 4.50, 3.00, 5.00, Texture_A_Dark, Texture_A_Light)}
//-------------------------------------------------- end of coordinate axes


// ground -----------------------------------------------------------------
//---------------------------------<<< settings of squared plane dimensions
#declare RasterScale = 1.0;
#declare RasterHalfLine  = 0.035;  
#declare RasterHalfLineZ = 0.035; 
//-------------------------------------------------------------------------
#macro Raster(RScale, HLine) 
       pigment{ gradient x scale RScale
                color_map{[0.000   color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,0>*0.6]
                          [1.000   color rgbt<1,1,1,0>*0.6]} }
 #end// of Raster(RScale, HLine)-macro    
//-------------------------------------------------------------------------
    

plane { <0,1,0>, 0    // plane with layered textures
        texture { pigment{color White*1.1}
                  finish {ambient 0.45 diffuse 0.85}}
        texture { Raster(RasterScale,RasterHalfLine ) rotate<0,0,0> }
        texture { Raster(RasterScale,RasterHalfLineZ) rotate<0,90,0>}
        rotate<0,0,0>
      }              */
//------------------------------------------------ end of squared plane XZ

//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------     

#declare Baum1Textur =   
    texture{ T_Wood9     
                normal { wood 5 scale 0.05 turbulence 0.1 rotate<0,0,0> }
                finish { phong 0.25 reflection 0 } 
                rotate<0,0,0> scale .4 translate<0,0,0>
              } // end of texture

#declare Baum2Textur = 
    texture{ Rosewood                      
                rotate<0,0,90> scale 0.25
                normal { wood 5 scale 0.05 turbulence 0.1 rotate<0,0,0> }
                 finish { phong 0.25 reflection 0}
               } // end of texture -------------------------------------

#declare Baum3Textur =
    texture{ T_Wood30    
                normal { wood 0.5 scale 0.05 turbulence 0.0 rotate<0,0,0> }
                finish { phong 0.25 reflection 0} 
                rotate<0,0,0> scale 1 translate<0,0,0>
              } // end of texture 

#declare Baum4Textur =    
     texture{ Tan_Wood     
                normal { wood 0.5 scale 0.3 turbulence 0.1}
                finish { phong 0.25 reflection 0} 
                rotate<60,0,45> scale 0.05
              } // end of texture 


#declare BaumHoehe = 2.6;

#declare Baum1 =
union{ 
    
    cylinder{ <0,0,0>,<0,2.0,0>,0.2
                texture{Baum1Textur}    
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
    } // end of cylinder --------------------------------------
    
    sphere { <0,2,0>, 1. 

                texture{ pigment{ //agate            // use an agate-like texture
                                  agate //0.5      // for use with normal{} (0...1 or more)
                                  agate_turb 1  // can alter turbulence [1.0]
                                  
                                 color_map {
                                    [0  color DarkGreen]
                                    //[0.3  color LimeGreen]
                                    //[0.5  color DarkGreen]
                                    //[0.7  color Green]
                                    [1  color Green*0.75]}
                                          scale 0.25      }
                 normal { spotted 25 scale 0.10 }
                 finish { phong 0.25 reflection 0 }
               } // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
       }  // end of sphere -----------------------------------
       
       cone { <0,-0.4,0>,0.3,<0,0.0,0>,0.2 
        texture{Baum1Textur} 
        scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
     } // end of cone -------------------------------------
     
    translate <0,0.4,0> 
} // --------------------------------------------------- end of Tree  

#declare Baum2 =
union{ 
    
    cylinder{ <0,0,0>,<0,2.0,0>,0.2
                texture{Baum2Textur}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
    } // end of cylinder --------------------------------------
    
    sphere { <0,2,0>, 1.         
                texture{ pigment{ //agate            // use an agate-like texture
                                  agate //0.5      // for use with normal{} (0...1 or more)
                                  agate_turb 1  // can alter turbulence [1.0]
                                  
                                 color_map {
                                    [0  color DarkGreen*0.5]
                                    //[0.3  color LimeGreen]
                                    //[0.5  color DarkGreen]
                                    //[0.7  color Green]
                                    [1  color Green*0.75*0.5]}
                                          scale 0.25      }
                 normal { spotted 25 scale 0.10 }
                 finish { phong 0.25 reflection 0 }
               } // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
       }  // end of sphere ----------------------------------- 

        cone { <0,-0.4,0>,0.3,<0,0.0,0>,0.2 
        texture{Baum2Textur}
        scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
     } // end of cone -------------------------------------
     
    translate <0,0.4,0>

} // --------------------------------------------------- end of Tree

#declare Baum3 =
union{ 
    
    cylinder{ <0,0,0>,<0,2.0,0>,0.2
                        texture{Baum3Textur} 
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
    } // end of cylinder --------------------------------------
    
    sphere { <0,2,0>, 1. 

                texture{ pigment{ //agate            // use an agate-like texture
                                  agate //0.5      // for use with normal{} (0...1 or more)
                                  agate_turb 1  // can alter turbulence [1.0]
                                  
                                 color_map {
                                    [0  color LimeGreen]
                                    //[0.3  color LimeGreen]
                                    //[0.5  color DarkGreen]
                                    //[0.7  color Green]
                                    [1  color Green*0.75*0.5]}
                                          scale 0.25      }
                 normal { spotted 25 scale 0.10 }
                 finish { phong 0.25 reflection 0 }
               } // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
       }  // end of sphere ----------------------------------- 

        cone { <0,-0.4,0>,0.3,<0,0.0,0>,0.2 
         texture{Baum3Textur}
        scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
     } // end of cone -------------------------------------
     
    translate <0,0.4,0>

} // --------------------------------------------------- end of Tree   

#declare Baum4 =
union{ 
    
    cylinder{ <0,0,0>,<0,2.0,0>,0.2
                   texture{Baum4Textur}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
    } // end of cylinder --------------------------------------
    
    sphere { <0,2,0>, 1. 

                texture{ pigment{ //agate            // use an agate-like texture
                                  agate //0.5      // for use with normal{} (0...1 or more)
                                  agate_turb 1  // can alter turbulence [1.0]
                                  
                                 color_map {
                                    [0  color DarkGreen*0.6]
                                    //[0.3  color LimeGreen]
                                    //[0.5  color DarkGreen]
                                    //[0.7  color Green]
                                    [1  color Green*0.75*0.6]}
                                          scale 0.25      }
                 normal { spotted 25 scale 0.10 }
                 finish { phong 0.25 reflection 0 }
               } // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
       }  // end of sphere ----------------------------------- 
      
      cone { <0,-0.4,0>,0.3,<0,0.0,0>,0.2 
         texture{Baum4Textur}
        scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
     } // end of cone -------------------------------------
     
    translate <0,0.4,0>


} // --------------------------------------------------- end of Tree

/*object { Baum1 translate <-5,0,0>}
object { Baum2 translate <-2,0,0>}  
object { Baum3 translate <1,0,0> }
object { Baum4 translate <4,0,0> }
                                       */