/*#include "hintergrund.pov"

camera{Camera_0} */  

#local ZylinderStoff =
   texture { pigment{ color rgb< 0.02, 0.02, 0.02> } //  color Black
                // normal { bumps 0.5 scale 0.05 }                
                   finish { ambient color<0.1,0.1,0.1> ambient 0.5 phong 1 phong_size 10  }
           } // end of texture
           
#local ZylinderStoffHell =
   texture { pigment{ color rgb< 0.02, 0.02, 0.02>*2 } //  color Black
                // normal { bumps 0.5 scale 0.05 }                
                   finish { ambient color<0.1,0.1,0.1> ambient 0.5 phong 1 phong_size 10  }
           } // end of texture             

     
#declare ZylinderHoehe = 1;
#declare Zylinder =
    difference
    {    
    
    
     union
     {              
      //Hutkrempe
      difference
      {
        cylinder { <0,0,0>,<0,0.20,0>, 1.8 
    
            texture {ZylinderStoff} // end of texture
    
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
         cylinder { <0,0.1,0>,<0,0.30,0>, 1.6 
    
            texture {ZylinderStoff} // end of texture
    
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------          
            
       }//Ende von Difference            
     
      //Hauptzylinder
      cylinder { <0,0,0>,<0,2.00,0>, 1 

           texture {ZylinderStoff} // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
      
      //Hutband   
      cylinder { <0,0.8,0>,<0,1.2,0>, 1.01 

           texture {ZylinderStoffHell} // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------   
      } //Ende von union     
         
          cylinder { <0,-0.1,0>,<0,1.90,0>, 0.9 

           texture {ZylinderStoff} // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
     scale 0.5     
     translate<0,-ZylinderHoehe/2,0>
     //rotate<90,-45,0> translate<0,2,0>
      
    }//Ende von difference
    
    
//object{Zylinder translate<0,ZylinderHoehe/2,0>}