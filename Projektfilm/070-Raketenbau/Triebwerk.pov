// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"     
//--------------------------------------------------------------------------
#include "shapes3.inc" //fuer gebogene Rohre
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 75      // front view
                            location  <0.0 , 3.0 ,-5.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera { angle 90   // bottom view
                            location  <1.0 , -1 ,-3>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 5.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_0}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>} 
          
           } //end of skysphere    */
// ground ------------------------------------------------------------------
/*
plane{ <0,1,0>, 0 
       texture{ pigment{ color rgb <0.7,0.5,0.3>}
              //normal { bumps 0.75 scale 0.025}
                finish { phong 0.1}
              } // end of texture
     } // end of plane
*/
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
        
#declare gestreiftesMetall = 
    texture
    {
       pigment
        {
        gradient <0,1,0>         // specify gradient direction vector
  
            color_map
            {
            [0.0 color rgb< 0.75, 0.5, 0.30>*0.5] 
            [0.7 color rgb<0.2, 0.2, 0.2>]
            [0.7 color rgb< 1, 1, 1>*0.10] 
            [1.0 color rgb< 1, 1, 1>*0.10]   
            /*
            [0.0 color rgb< 0.5, 0.75, 1.0> ] 
            [0.7 color rgb<0.4, 0.65, 0.9>]
            */

            }// Ende Colormap
        } //Ende Pigment
        finish { phong 0 reflection 0.00}

    }    
    
#declare GlanzMetall =
     texture{  T_Chrome_1A
               finish{ambient 0.3 diffuse 0.6 reflection 0.03 brilliance 4}
              //scale 0.5  
            } // end of texture ---------------------------    
            
#declare GlanzMetall2 =
     texture{  Bronze_Metal
               finish{ambient 0.3 diffuse 0.6 reflection 0.03 brilliance 4}
              //scale 0.5  
            } // end of texture ---------------------------

#declare Triebwerkduese =   
    union
    {
      difference
        {
         sphere
        {
         0,1
         texture { gestreiftesMetall scale 0.25}  
        }
        
         sphere
        {
         0,0.8
         texture{ GlanzMetall 
                } // end of texture --------------------------- 
         }
     
        box
        {
         <-2,-2,-2>, <2,0,2>  
         texture{ GlanzMetall 
                } // end of texture --------------------------- 
         }   
        scale <1,1.5,1>
        }
        
        /*torus { 1.0,0.05  translate<0,0.3,0>  
                texture { T_Chrome_1A finish { phong 1}} 
       
      } // end of torus  -------------------------------   */           

     }
                               
#declare HauptLeitung =
    union
    {
        cone
        {
            <0,0.75,0>, 0.75, <0,3,0>, 0.5
            texture{GlanzMetall}
            translate <0,0.5,0>
        }
    }  
    
#declare Rohr =  
    union
    {
         cylinder{<0,0,0>, <0,0.5,0>,0.15 rotate <0,0,45> texture{ GlanzMetall2 }}
         cylinder{<0,0,0>, <0,2,0>,0.15 rotate <0,0,-28> translate<-0.47,0.9,0> texture{ GlanzMetall2 }}
         
        
        object{ Segment_of_Torus ( 0.5, // radius major, 
                           0.15, // radius minor, 
                           70  // segment angle (in degrees)
                         ) //-----------------------------------
        texture { GlanzMetall2
                } // end of texture
        scale <1,1,1> rotate<90,180,45> translate<0,0.71,0>
      } // end of Torus_Segment(...) ---------------------------              
                    
    }
    

#declare Leitungen =
    union
    {                
        cylinder{<0,0,0>, <0,3,0>,0.05 texture{ GlanzMetall } translate <-0.65,0,-0.65>}
        cylinder{<0,0,0>, <0,3,0>,0.05 texture{ GlanzMetall } translate <0.65,0,-0.65>}
        cylinder{<0,0,0>, <0,3,0>,0.05 texture{ GlanzMetall } translate <0.65,0,0.65>}
        cylinder{<0,0,0>, <0,3,0>,0.05 texture{ GlanzMetall } translate <-0.65,0,0.65>}
        object{Rohr translate <-0.7,0.8,0> }
        object{Rohr translate <-0.7,0.8,0> rotate<0,90,0> }
        object{Rohr translate <-0.7,0.8,0> rotate<0,180,0> }
        object{Rohr translate <-0.7,0.8,0> rotate<0,-90,0> }  
    }                             

#declare Triebwerk = 
    union
    {
        object{ Triebwerkduese }
        object{ HauptLeitung } 
        object{ Leitungen}
    }

          /*
object{ Triebwerk }   

// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera { angle 75      // front view
                            location  <0.0 , 1.0 ,-7.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_0}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>} 
          
           } //end of skysphere
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
                          
                        */
                    