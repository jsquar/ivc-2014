// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"   
#include "KapselLuke.pov"
//------------------------------------------------------------------------
#declare Camera_0 = camera {/*ultra_wide_angle*/ angle 15      // front view
                            location  <0.0 , 1.0 ,-40.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera {/*ultra_wide_angle*/ angle 14   // diagonal view
                            location  <20.0 , 15.0 ,-20.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1 , 0.0>}
#declare Camera_2 = camera {/*ultra_wide_angle*/ angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera {/*ultra_wide_angle*/ angle 90        // top view
                            location  <0.0 , 3.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_1}
//------------------------------------------------------------------------
// sun -------------------------------------------------------------------
//light_source{<1500,2500,-2500> color White}
// sky -------------------------------------------------------------------
sky_sphere{ pigment{ gradient <0,1,0>
                     color_map{ [0   color rgb<1,1,1>         ]//White
                                [0.4 color rgb<0.14,0.14,0.56>]//~Navy
                                [0.6 color rgb<0.14,0.14,0.56>]//~Navy
                                [1.0 color rgb<1,1,1>         ]//White
                              }
                     scale 2 }
           } // end of sky_sphere 
//------------------------------------------------------------------------

//------------------------------ the Axes --------------------------------
//------------------------------------------------------------------------
#macro Axis_( AxisLen, Dark_Texture,Light_Texture) 
 union{
    cylinder { <0,-AxisLen,0>,<0,AxisLen,0>,0.05
               texture{checker texture{Dark_Texture } 
                               texture{Light_Texture}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{Dark_Texture}
         }
     } // end of union                   
#end // of macro "Axis()"
//------------------------------------------------------------------------
#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ, Tex_Dark, Tex_Light)
//--------------------- drawing of 3 Axes --------------------------------
union{
#if (AxisLenX != 0)
 object { Axis_(AxisLenX, Tex_Dark, Tex_Light)   rotate< 0,0,-90>}// x-Axis
 text   { ttf "arial.ttf",  "x",  0.15,  0  texture{Tex_Dark} 
          rotate<20,-45,0> scale 0.75 translate <AxisLenX+0.05,0.4,-0.10> no_shadow}
#end // of #if 
#if (AxisLenY != 0)
 object { Axis_(AxisLenY, Tex_Dark, Tex_Light)   rotate< 0,0,  0>}// y-Axis
 text   { ttf "arial.ttf",  "y",  0.15,  0  texture{Tex_Dark}    
          rotate<10,0,0> scale 0.75 translate <-0.65,AxisLenY+0.50,-0.10>  rotate<0,-45,0> no_shadow}
#end // of #if 
#if (AxisLenZ != 0)
 object { Axis_(AxisLenZ, Tex_Dark, Tex_Light)   rotate<90,0,  0>}// z-Axis
 text   { ttf "arial.ttf",  "z",  0.15,  0  texture{Tex_Dark}
          rotate<20,-45,0> scale 0.85 translate <-0.75,0.2,AxisLenZ+0.10> no_shadow}
#end // of #if 
} // end of union
#end// of macro "AxisXYZ( ... )"
//------------------------------------------------------------------------

#declare Texture_A_Dark  = texture {
                               pigment{ color rgb<1,0.45,0>}
                               finish { phong 1}
                             }
#declare Texture_A_Light = texture { 
                               pigment{ color rgb<1,1,1>}
                               finish { phong 1}
                             }

object{ AxisXYZ( 4.50, 3.00, 5.00, Texture_A_Dark, Texture_A_Light)}
//-------------------------------------------------- end of coordinate axes


// ground -----------------------------------------------------------------
//---------------------------------<<< settings of squared plane dimensions
#declare RasterScale = 1.0;
#declare RasterHalfLine  = 0.035;  
#declare RasterHalfLineZ = 0.035; 
//-------------------------------------------------------------------------
#macro Raster(RScale, HLine) 
       pigment{ gradient x scale RScale
                color_map{[0.000   color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,0>*0.6]
                          [0+HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,1>]
                          [1-HLine color rgbt<1,1,1,0>*0.6]
                          [1.000   color rgbt<1,1,1,0>*0.6]} }
 #end// of Raster(RScale, HLine)-macro    
//-------------------------------------------------------------------------
    

plane { <0,1,0>, 0    // plane with layered textures
        texture { pigment{color White*1.1}
                  finish {ambient 0.45 diffuse 0.85}}
        texture { Raster(RasterScale,RasterHalfLine ) rotate<0,0,0> }
        texture { Raster(RasterScale,RasterHalfLineZ) rotate<0,90,0>}
        rotate<0,0,0>
      }
//------------------------------------------------ end of squared plane XZ

//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
                                                                                               

#declare DensMap = density {
        radial  rotate y*90 translate z*-5
        translate <0,0,-15*clock>
        warp {turbulence 0.8 octaves 3 lambda 2.5}
        translate <0,0,15*clock>
        scale <1,1,1> 
        colour_map {
                [0.49 rgb 0]
                [0.495 rgb 1]
                [0.5 rgb 4]
                [0.505 rgb 1]
                [0.51 rgb 0]
        }
}
#declare ColMap = density {
        radial rotate y*90 translate z*-5
        translate <0,0,-15*clock>
        warp {turbulence 1.2 octaves 3 lambda 2.5}
        translate <0,0,15*clock>
        scale <1,1,1> 
        colour_map {
                [0.495 Yellow]
                [0.5 White]
                [0.505 Yellow]
        }
}
#declare DensEnding = density {
        spherical scale 10
        colour_map {
                [0 rgb 0*<1,0,0>]
                [0.5 rgb <1,.5,0>*.5]
                [.75 rgb <1,1,0>] 
                [.875 rgb 1] 
                [1 rgb <-1,0,1>]
        }
}

#declare DensMap2 = density {
        radial rotate y*90 translate z*-3
        translate <0,0,-25*clock>
        warp {turbulence 1.3 octaves 3 }
        translate <0,0,25*clock>
        scale <1,1,1>
        colour_map {
                [0.45 rgb 0]
                [0.48 rgb 0.4]
                [0.5 rgb 1]
                [0.52 rgb 0.4]
                [0.55 rgb 0]
        }
}
#declare ColMap2 = density {
        radial rotate y*90 translate z*-3
        translate <0,0,-25*clock>
        warp {turbulence 1.3 octaves 3 }
        translate <0,0,25*clock>
        scale <1,1,1>
        colour_map {
                [0.47 Red]
                [0.5 Orange]
                [0.53 Red]
        }
}
#declare DensEnding2 = density {
        spherical scale <5,5,10>
        translate <0,0,-15*clock>
        warp {turbulence 0.5}
        translate <0,0,15*clock>
        colour_map {
                [0 rgb 0]
                [0.1 rgb 1]
                [.875 rgb 1] 
                [1 rgb <-1,-1,1>]
        }
}

#declare Flame =
union {
cylinder {
        0,z*10,3
                      
        pigment {Clear}
        hollow
        interior {
                media {
                        emission 1
                        /*intervals 5
                        samples 2, 30
                        confidence 0.9999
                        variance 1/1000*/
                        density {ColMap}
                        density {DensMap}
                        density {DensMap rotate z*90}
                        density {DensEnding}
                scale .8
                }
                media {
                        emission .75
                        /*intervals 5
                        samples 2, 30
                        confidence 0.9999
                        variance 1/1000*/
                        density {ColMap2}
                        density {DensMap2}
                        density {DensMap2 rotate z*90}
                        density {DensEnding2}
                scale .8
                }
        
        }
        scale <1,1,3>/.8
}
cylinder {
        0,z*10,3
                      
        pigment {Clear}
        hollow
        interior {
                media {
                        absorption rgb .675
                        //scattering { 2, rgb .5 extinction 1 }                        
                        density {bozo
                        translate <0,0,-5*clock>
                        warp {turbulence 0.2 octaves 1 lambda .5}
                        translate <0,0,5*clock> 
                        scale <1,1,1/3>*.75                        
                        color_map {
                        [0 rgb 0]
                        [1 rgb 1]         
                        }
                        }
                        density {
                        spherical scale <5,5,10>
                        translate <0,0,-5*clock>
                        warp {turbulence 0.5}
                        translate <0,0,5*clock>
                        color_map {
                        [0 rgb 0]
                        [1 rgb 1]         
                        }
                        }
                scale .8
                }
        }
        scale <.675,.675,2>/.8
        rotate -x*10
        translate <0,0,20>
}

}

object{Flame scale 0.25 translate<0,0,-3>}