/*#include "hintergrund.pov"   

camera{Camera_1}  */

#declare Window_Glass =
texture{
  pigment{ rgbf< 1.0, 0.15, 0.0,0.9>}
  finish { diffuse 0.1
           reflection 0.1
           specular 0.8
           roughness 0.0003
           phong 1
           phong_size 400}
  } // end of texture --------------    
                                          

#declare SignallampeHoehe = 2;                                          

#declare Lampengehaeuse =
    union
    {
        cone{ <0,0,0>,1.00,<0,2,0>,0.800 

        texture{ Window_Glass} // end of texture
        scale <1,1,1> rotate<0,0,0> translate<0,0.0001,0> 
        } // end of cone ----------------------------------- 
        
        cylinder { <0,0,0>,<0,0.20,0>, 0.95 

                    texture { pigment{ color rgb< 1, 1, 1>*0.20 } //  color Black
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 }
                 } // end of texture    

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
         translate<0,-SignallampeHoehe/2,0>

    }
    
#local Reflektion = 
             texture{ Silver_Metal
                  finish { phong 1 }
                  scale 0.5
                } // end of texture ---------------------------  
                  
   
#declare Rotationsobjekt =
    union
    {
        cylinder { <0,0,0>,<0,0.600,0>, 0.20 

           texture { Reflektion }

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
         
         difference
         {
          sphere { <0,0.5,0>, 0.5 

                texture { Reflektion }

            scale<0.5,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
             }  // end of sphere ----------------------------------- 
           
           sphere { <0,0.5,0>, 0.4 

                texture { Reflektion }

            scale<0.5,1,1>  rotate<0,0,0>  translate<0,0.5,0>  
             }  // end of sphere -----------------------------------   
             
             box { <0.00, 0.00, -1.00>,< 2.00, 2.00, 1.00>   

                texture { Reflektion }

      scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
    } // end of box --------------------------------------


         } //Ende Difference 
         translate<0,-SignallampeHoehe/2,0>
    }
                        
/*                        
object{Lampengehaeuse }          
object{ Rotationsobjekt }
*/