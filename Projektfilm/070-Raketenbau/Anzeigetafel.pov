/*#include "hintergrund.pov"

camera{Camera_1}  
*/

                    
#declare AnzeigetafelHoehe = 2;
                                  
#declare Window_Glass =
texture{
  pigment{ rgbf< 1.0, 0.75, 0.75,1>}
  finish { diffuse 0.1
           reflection 0.1
           specular 0.8
           roughness 0.0003
           phong 1
           phong_size 400}
  } // end of texture --------------
                                    
#local Stuetze = 
      union
      {                                                      
        box { <0.200, 0.0, 0.1>,< -0.200, 2.00, -0.1>   
        texture { pigment{ color rgb< 0.0, 0.0, 0.5>*0.1 } //  dark blue 
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 reflection 0.00}
                 } // end of texture 

        scale <1,1,1> rotate<0,0,0> translate<0,0,-2.5> 
        } // end of box --------------------------------------
      
        box { <0.200, -0.0, 0.1>,< -0.200, 1.414,  -0.1>   
        texture { pigment{ color rgb< 0.0, 0.0, 0.5>*0.1 } //  dark blue 
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 reflection 0.00}
                 } // end of texture 
        
        scale <1,1,1> rotate<45,0,0> translate<0,1.2,-2.5> 
        } // end of box --------------------------------------
      }            

#declare Anzeigetafel =
union
{
    difference
    {        
        union
        {
        box { <-0.200, 0.00, -3.00>,< 0.200, 2.00, 3.00>   
                 texture { pigment{ color rgb< 0.0, 0.0, 0.5>*0.1 } //  dark blue 
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 reflection 0.00}
                 } // end of texture 


        scale <1,1,1> rotate<0,0,0> translate<0,2,0> 
        } // end of box --------------------------------------
        
        object{Stuetze}
        object{Stuetze rotate<0,180,0>}
        }//Ende Union
                                                              
        box { <0.300, 0.300, -2.700>,< -0.300, 1.700, 2.700>   
        texture{ pigment{ checker
            color rgb <1, 1, 1>*0.2,
            color rgb <1, 1, 1>*0.4
                } scale 0.25}

        scale <1,1,1> rotate<0,0,0> translate<0,2,0> 
        } // end of box --------------------------------------
     translate<0,-1,0>
    }//Ende Difference
    box { <0.100, 0.300, -2.700>,< -0.100, 1.700, 2.700>   
        texture{Window_Glass}

        scale <1,1,1> rotate<0,0,0> translate<0,1,0> 
        } // end of box --------------------------------------  
}                         
    
    
       
//object{ Anzeigetafel translate<0,AnzeigetafelHoehe/2,0>}
//object{ Stuetze } 
