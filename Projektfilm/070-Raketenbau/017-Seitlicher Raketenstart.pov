// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 

#include "Rakete.pov"
#include "Typ.pov"
#include "Startschalter.pov"
#include "Launchpad.pov"  
#include "Signallampe.pov"
#include "Anzeigetafel.pov" 
#include "Wald.pov"
#include "Berg_gross.pov"
//#include "Sonne.pov"  
#include "Hammer.pov"
#include "Seven_Segment_LCD.inc"

//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {angle 75      // front view
                            location  <0.0 , 10.0 ,-10.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , 0.0>}
#declare Camera_1 = camera {angle 90      
                            location  <10 ,1.5+ LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) ,-19>
                            right     x*image_width/image_height
                            look_at   <0 , LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) , -20>}                                                                                 
#declare Camera_2 = camera {angle 90 // right side view
                            location  <10.0 , 10.0 , -1.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , -1.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 20.0 ,-20.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , -20.0>}
#declare Camera_4 = camera { angle 90        // top view
                            location  <15.0 , 10.0 ,-0.00>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}    
#declare Camera_5 = camera { angle 90        // top view
                            location  <100.0 , 50.0 ,-100.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}   
                        
#declare Camera_10 = camera {angle 90
                            location  <10 ,LaunchpadHoehe*10/4+AnzeigetafelHoehe*1.5+2 ,6>
                            right     x*image_width/image_height
                            look_at   <0 , LaunchpadHoehe*10/4+AnzeigetafelHoehe*1.5 , -0>}                                                                                                            
camera{Camera_10}
//camera{Camera_2 translate<0,0,-20> }

// sun ---------------------------------------------------------------------
light_source{<-1500,2000,-2500> color White}

// sky -------------------------------------------------------------- 
plane{<0,1,0>,1 hollow  
       texture{ pigment{ bozo turbulence 0.92
                         color_map { [0.00 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.50 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.70 rgb <1,1,1>]
                                     [0.85 rgb <0.25,0.25,0.25>]
                                     [1.0 rgb <0.5,0.5,0.5>]}
                        scale<1,1,1.5>*2.5  translate< 0,0,0>
                       }
                finish {ambient 1 diffuse 0} }      
       scale 10000}
// fog on the ground -------------------------------------------------
fog { fog_type   2
      distance   100
      color      White  
      fog_offset 0.1
      fog_alt    1.5
      turbulence 1.8
    }

// ground ------------------------------------------------------------
difference{
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.72 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane
      
      cylinder { <0,-30,0>,<0,10.00,0>, 10 
                    texture { T_Grnt9
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 1 
                 } // end of texture 

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}//Ende von Difference
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------    
//Triebwerkfeuer, Quelle: http://www.f-lohmueller.de/pov_tut/interior/interior_44d.htm 
#declare Random_1 = seed (int(1337*clock));

#local Flame__Shape =
merge{    // egg
intersection{
   sphere{<0,0,0>,1}
   box{<-1,-1,-1>,< 1,1  ,1>}
   scale <1,2.5,1>}
intersection{
   sphere{<0,0,0>,1}
   box{<-1,-1 ,-1>,< 1,0+0.01,1>}
   }
} // end of merge
// -----------------------------------------
#local C_Flame =
object{
  Flame__Shape
  hollow
  pigment{ color rgbf<1,1,1,1>}// clear!
  finish { ambient 0 diffuse 0 }
  interior {
    media {
      emission color rgb<1.0,0.75,0.1>*1.75
      intervals 9
      samples 1, 20
      confidence 0.9999
      variance 1/100
      density{ spherical
               ramp_wave
               turbulence 0.15*10
               color_map {
               [0.0 color rgb <0.0,0.0,0.0>]
               [0.6 color rgb <0.8,0.3,0.0>]
               [0.9 color rgb <1.0,0.8,0.3>]
               [1.0 color rgb <1.0,1.0,0.1>]
                         } // end color_map
            scale <1,2.5,1>*(rand( Random_1 )+0.5)
          } // end of density
    }//end of media
    media{
    emission color rgb<1.0, 0.75, 0.1>*2.75
    intervals 3
    samples 32
    method 3
    aa_threshold 0.1 aa_level 5
    density{ crackle  turbulence 0.3*10
             scale 0.75 translate<0,24,0>
             color_map {
                    [0 rgb 0]
                    [0.00 rgb 0]
                    [0.07 rgb 1]
                    [0.10 rgb 1]
                    [0.12 rgb 0]
                    [1.00 rgb 0]
              } // end color_map
           } // end density
     density{ spherical
              color_map {
                    [0 rgb 0]
                    [0.7 rgb 1]
                    [1 rgb 1]
                } // end color_map
            } // end density
           scale<1,2.5,1>*0.5
       translate<0,0.01,0>
     }// end media
}//ende von interior
}

#local Typfaktor = 0.5;
object{Launchpad translate<0,-LaunchpadHoehe*3/4,0,> scale 10 }   


    difference{
    object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> scale 10} 
    cylinder { <0,-2,0>,<0,20.00,0>, 3
            texture { pigment { color rgb<1,1,1> }}
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder ------------------------------------- 
        translate<0,-2,-0.5-9>            
    }
                                                        
    difference{
    object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> rotate<0,180,0> scale 10} 
    cylinder { <0,-20,0>,<0,20.00,0>, 3
            texture { pigment { color rgb<1,1,1> }}
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
        translate<0,-2,0.5+9>                        
    }
  
union
{
    object{Rakete scale<1,1,1>*2 rotate<0,90,0> } 
    object{C_Flame scale <2,2.5,2> rotate<180,0,0> translate<2,-3,-2>}
    object{C_Flame scale <2,2.5,2> rotate<180,0,0> translate<-2,-3,-2>}  
    object{C_Flame scale <2,2.5,2> rotate<180,0,0> translate<-2,-3,2>}  
    object{C_Flame scale <2,2.5,2> rotate<180,0,0> translate<2,-3,2>} 
    // create a regular point light source
    light_source {
        0                  // light's position (translated below)
        color rgb <1,1,0>    // light's color 
        fade_distance 10
        fade_power 2
        translate <0,-3,0>
}
   
    translate<0,-34+pow((8.950*clock),2)*0.6,0>
}    
 
    //Rechte Lampe
    object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>}
    object{Rotationsobjekt rotate<0,90+360*(clock)*1.5,0> translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>} 
    light_source{ <11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0> color rgb <1,0,0>*4   
              spotlight
              point_at<0,0,0>
              radius 1  // hotspot
              tightness 100
              falloff 60
              translate<0,LaunchpadHoehe*10/4+1,0>
              translate<-11,0,0> rotate<0,90+360*(clock)*1.5,0> translate<11,0,0> 
            }   
            
    cone {
        <11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0>,0,<-0,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2+2,0>,4
        pigment {rgbt <1,0,0,0.95>}
        interior {
                media {
                        emission 0.67/3 // intensity of air glow
                        density {
                                cylindrical
                                }
                }
        } 
    hollow
    translate<-11,0,0> rotate<0,90+360*(clock)*1.5,0> translate<11,0,0>}       
    
    //Linke Lampe
    object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}
    object{Rotationsobjekt rotate<0,90+360*(clock)*1.5,0> translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}
    light_source{ <-11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0> color rgb <1,0,0>*4   
              spotlight
              point_at<0,0,0>
              radius 1  // hotspot
              tightness 100
              falloff 60
              translate<0,LaunchpadHoehe*10/4+1,0>
              translate<11,0,0> rotate<0,90+360*(clock)*1.5,0> translate<-11,0,0> 
            }   
            
    cone {
        <-11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0>,0,<-0,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2+2,0>,4
        pigment {rgbt <1,0,0,0.95>}
        interior {
                media {
                        emission 0.67/3 // intensity of air glow
                        density {
                                cylindrical
                                }
                }
        } 
    hollow
    translate<11,0,0> rotate<0,90+360*(clock)*1.5,0> translate<-11,0,0>}


//Signaltafel und Startknopf
object{Anzeigetafel scale 1 translate<0,AnzeigetafelHoehe/2,0> scale 1 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-22>}
object{Startschalter scale 0.7 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-20>}
object{Startknopf scale 0.7 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4-StartknopfHoehe*0.98,-20>} 

#declare Active_Texture = 
//    texture { pigment{ color rgb< 1, 0.0, 0>*1.2 } //  color Red
    texture { pigment{ color rgb< 0.25, 0.5, 0>*1.7  } //  color Green
//  texture { pigment{ color rgb< 1, 1, 1>*1.1  } //  color White
                   finish { ambient 0.9 diffuse 0.1 phong 1}
              } // end of texture 
#declare Inactive_Texture = 
    texture { pigment{ color rgb< 1, 1, 1>*0.35 } //  color gray
                   finish { phong 1 reflection 0.00}
              } // end of texture 
#declare Background_Texture =
    texture { pigment{ color rgb< 1, 1, 1>*0.05 } //  color nearly black
                   finish { phong 1 reflection 0.0}
              } // end of texture
              
#local Number = 0; //0~99

#local Num_10 = int ( Number/10);
#local Num_1  = int (mod(Number,10)); 

#if(Num_10=0) #declare Num_10=0; #end                 
                                          //4 Sekunden = 100 Bilder
union
{
object{ Seven_Segment_LCD(          
         Num_1, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<0.88+0.42,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------                                               
       
object{ Seven_Segment_LCD(          
         Num_10, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<0.44,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------

object{ Seven_Segment_LCD(          
         0, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         1, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<-0.44,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------           
       
object{ Seven_Segment_LCD(          
         0, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<-(0.88+0.42),LaunchpadHoehe*10/4,-22.1>
       } // ------------------------
translate<0,0,22.1> rotate<0,180,0> translate<0,0,-22.1>
}//Ende union       
                 
          
//Umgebung 
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,90,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,-90,0>}


object{Berg scale 3 rotate<0,-90,0> translate<0,-10,0> scale <2,3,4> translate<-200,0,-10>}      

//object{Sonne}









