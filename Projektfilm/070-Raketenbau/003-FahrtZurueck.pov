// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 

#include "Rakete.pov"
#include "Typ.pov"
#include "Startschalter.pov"
#include "Launchpad.pov"  
#include "Signallampe.pov"
#include "Anzeigetafel.pov" 
#include "Wald.pov"
#include "Berg_gross.pov"
//#include "Sonne.pov"  
#include "Hammer.pov"

//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {angle 75      // front view
                            location  <0.0 , 10.0 ,-10.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , 0.0>}
#declare Camera_1 = camera { angle 90   // diagonal view
                            location  <2.0 , 2.5 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_2 = camera {angle 90 // right side view
                            location  <10.0 , 10.0 , -1.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , -1.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 20.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_4 = camera { angle 90        // top view
                            location  <15.0 , 10.0 ,-0.00>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}    
#declare Camera_5 = camera { angle 90        // top view
                            location  <100.0 , 50.0 ,-100.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}   
#declare Camera_10 = camera {angle 90      
                            location  <4+6*clock , LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) ,-3-6*clock>
                            right     x*image_width/image_height
                            look_at   <4-4*clock , LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) , 0.0>}                                                                                 
//camera{Camera_0 translate<0.3,-0.2,1.5>}
camera{Camera_10 translate<-1.5,0.5,1>}

// sun ---------------------------------------------------------------------
light_source{<-1500,2000,-2500> color White}

// sky -------------------------------------------------------------- 
plane{<0,1,0>,1 hollow  
       texture{ pigment{ bozo turbulence 0.92
                         color_map { [0.00 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.50 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.70 rgb <1,1,1>]
                                     [0.85 rgb <0.25,0.25,0.25>]
                                     [1.0 rgb <0.5,0.5,0.5>]}
                        scale<1,1,1.5>*2.5  translate< 0,0,0>
                       }
                finish {ambient 1 diffuse 0} }      
       scale 10000}
// fog on the ground -------------------------------------------------
fog { fog_type   2
      distance   100
      color      White  
      fog_offset 0.1
      fog_alt    1.5
      turbulence 1.8
    }

// ground ------------------------------------------------------------
difference{
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.72 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane
      
      cylinder { <0,-10,0>,<0,10.00,0>, 3 
                    texture { T_Grnt9
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 1 
                 } // end of texture 

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}//Ende von Difference
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------  
#local Typfaktor = 0.5;
object{Launchpad translate<0,-LaunchpadHoehe*3/4,0,> scale 10 }   


difference{
object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> scale 10} 
cylinder { <0,-2,0>,<0,20.00,0>, 3
           texture { pigment { color rgb<1,1,1> }}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}
                                                     
difference{
object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> rotate<0,180,0> scale 10} 
cylinder { <0,-20,0>,<0,20.00,0>, 3
           texture { pigment { color rgb<1,1,1> }}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}

object{Rakete scale<1,1,1>*2 rotate<0,90,0> translate<0,-34,0>} 
 
//Rechte Lampe
object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>}
object{Rotationsobjekt translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>}

//Linke Lampe
object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}
object{Rotationsobjekt translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}

//Signaltafel und Startknopf
object{Anzeigetafel scale 1 translate<0,AnzeigetafelHoehe/2,0> scale 1 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-22>}
object{Startschalter scale 1 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-20>}
object{Startknopf scale 1 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-20>}   
          
//Typ                                                  
union{ 
#if(clock<=0.2)
    object{Kopf rotate<30*clock/0.2,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
    object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<30*clock/0.2,0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
#end
#if(clock>0.2 & clock<0.8)    
        object{Kopf rotate<29,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
        object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<29,0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
#end
#if(clock>=0.8)
        object{Kopf rotate<30*(1-clock)/0.2,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
        object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<30*(1-clock)/0.2,0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
#end
object{Oberkoerper translate<0,BeinHoehe,0> scale Typfaktor }  

object{Arm rotate <0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }
object{Arm rotate <0,0,-60> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor }

object{Bein translate<UnterkoerperBreite/2-0.4,BeinHoehe,0> scale Typfaktor }
object{Bein translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0> scale Typfaktor }
 rotate<0,90,0> translate<3.5,LaunchpadHoehe*10/4,0>
}        

//Hammer
object{Hammer translate<Armlaenge,0,0> rotate<0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor rotate<0,90,0> translate<3.5,LaunchpadHoehe*10/4,0>}                

//Umgebung 
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,90,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,-90,0>}


object{Berg scale 3 rotate<0,-90,0> translate<0,-10,0> scale <2,3,4> translate<-200,0,-10>}      

//object{Sonne}









