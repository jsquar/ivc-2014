#include "untereStage.pov"
#include "obereStage.pov"
#include "Kapsel.pov"

//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
#declare RaketeHoehe = 100;

#declare Rakete =         
    union
    {
        object { untereStage translate <0,untereStageHoehe/2,0>}
        object { obereStage translate <0,untereStageHoehe + obereStageHoeheVerkuppelt - obereStageHoehe/2 , 0> }
        object { Kapsel translate <0,untereStageHoehe + obereStageHoeheVerkuppelt + KapselHoehe/2,0> }
    }
    
//object{ Rakete }   

