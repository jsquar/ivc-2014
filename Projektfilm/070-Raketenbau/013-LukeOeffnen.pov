// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 

#include "Rakete.pov"
#include "Typ.pov"
#include "Startschalter.pov"
#include "Launchpad.pov"  
#include "Signallampe.pov"
#include "Anzeigetafel.pov" 
#include "Wald.pov"
#include "Berg_gross.pov"
//#include "Sonne.pov"  
#include "Hammer.pov"
#include "Seven_Segment_LCD.inc"

//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {angle 75      // front view
                            location  <0.0 , 10.0 ,-10.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , 0.0>}
#declare Camera_1 = camera {angle 90      
                            location  <10 ,1.5+ LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) ,-19>
                            right     x*image_width/image_height
                            look_at   <0 , LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) , -20>}                                                                                 
#declare Camera_2 = camera {angle 90 // right side view
                            location  <5.0 ,1.5+ LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe)-1 , -2.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.5+ LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe)-1 , -2.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 10.0 ,-4.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , -4.0>}
#declare Camera_4 = camera { angle 90        // top view
                            location  <15.0 , 10.0 ,-0.00>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}    
#declare Camera_5 = camera { angle 90        // top view
                            location  <100.0 , 50.0 ,-100.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}   
#declare Camera_10 = camera {angle 90      
                            location  <3 ,1.5+ LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) ,-4.75>
                            right     x*image_width/image_height
                            look_at   <0 , LaunchpadHoehe*10/4+0.5*(ArmHoehe+BeinHoehe) , -3.75>}                                                                                 
camera{Camera_10}
//camera{Camera_2 translate<0,0,-20> }

// sun ---------------------------------------------------------------------
light_source{<-1500,2000,-2500> color White}

// sky -------------------------------------------------------------- 
plane{<0,1,0>,1 hollow  
       texture{ pigment{ bozo turbulence 0.92
                         color_map { [0.00 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.50 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.70 rgb <1,1,1>]
                                     [0.85 rgb <0.25,0.25,0.25>]
                                     [1.0 rgb <0.5,0.5,0.5>]}
                        scale<1,1,1.5>*2.5  translate< 0,0,0>
                       }
                finish {ambient 1 diffuse 0} }      
       scale 10000}
// fog on the ground -------------------------------------------------
fog { fog_type   2
      distance   100
      color      White  
      fog_offset 0.1
      fog_alt    1.5
      turbulence 1.8
    }

// ground ------------------------------------------------------------
difference{
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.72 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane
      
      cylinder { <0,-10,0>,<0,10.00,0>, 3 
                    texture { T_Grnt9
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 1 
                 } // end of texture 

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}//Ende von Difference
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------   

#declare KapselLukeZylinder_eigen =
    union
    {          
        difference
        {
            cylinder { <0,-.75,0>,<0,.7500,0>, 1.50 
                material{LukeAussenfarbe}         

                scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
            cylinder { <0,-1,0>,<0,1.0,0>, 1.40 
                material{LukeInnenfarbe}
                scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
           cylinder { <0,0,-3>,<0,0,3.00>,0.150 
            material{LukeGlas}
            scale <1,1,1> rotate<0,90+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------

         }//Ende von Difference
         cylinder { <0,0,1.37>,<0,0,1.520>,0.150 
            material{LukeGlas2}
            scale <1,1,1> rotate<0,90+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------
         difference
         {
            cylinder { <-1.55,0,0>,<-1.3,0,0>, 0.20
                 texture { LukeGlasRing } // end of texture
                scale <1,1,1> rotate<0,180+45/2,0> translate<0,0.3,0>
                } // end of cylinder  ------------------------------------
                
            cylinder { <-1.6,0,0>,<1.1,0,0>,0.1510 
            texture{LukeGlasRing}
            scale <1,1,1> rotate<0,180+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------    
         }//Ende von Difference
        
        //Rad beim Luke�ffnen drehen
        #if(clock<=0.2)
                object{Ventilrad scale 0.15 rotate<-90,0,0> rotate<0,0,0> translate<0,-0.2,-1.65> rotate<0,-90+45/2,0> }
        #end
        #if((clock>0.2) & (clock<=0.4))
                object{Ventilrad scale 0.15 rotate<-90,0,0> rotate<0,0,-45*(clock-0.2)/0.2> translate<0,-0.2,-1.65> rotate<0,-90+45/2,0> }
        #end   
        #if(clock > 0.4)
                object{Ventilrad scale 0.15 rotate<-90,0,0> rotate<0,0,45> translate<0,-0.2,-1.65> rotate<0,-90+45/2,0> }
        #end

    }//Ende von union

#declare KapselLuke_eigen =
    object{ Segment_of_Object( KapselLukeZylinder_eigen, 45 )   
        texture{ pigment{ color rgb<1,1,1>} 
                 finish { phong 1}
               } // end of texture 
        scale <1,1,1> rotate<0,45+45/2,0> translate<0,0,1.5>
      }
      
#local Typfaktor = 0.5;
object{Launchpad translate<0,-LaunchpadHoehe*3/4,0,> scale 10 }   


difference{
object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> scale 10} 
cylinder { <0,-2,0>,<0,20.00,0>, 3
           texture { pigment { color rgb<1,1,1> }}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}
                                                     
difference{
object{Luke translate<0,-LaunchpadHoehe*3/4-LukeHoehe,0> rotate<0,180,0> scale 10} 
cylinder { <0,-20,0>,<0,20.00,0>, 3
           texture { pigment { color rgb<1,1,1> }}
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
}
    
//Luke �ffnen
#if(clock<=0.2)
    object{Rakete scale<1,1,1>*2 rotate<0,90,0> translate<0,-34,0>} 
#end
#if(clock>0.2)
        union
    {      
        difference
        {
            cone { <0,0,0>,1.5,<0,2.5,0>,0.5 
                  material { RumpfTextur } // end of texture
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------
            cone { <0,0.1,0>,1.4,<0,2.40,0>,0.4 
                  texture { pigment{ color rgb<1,0.60,0.0>}
                    finish { phong 1 reflection{ 0.00 metallic 0.00} }} // end of texture
                     scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------   
            
            box { <0,0,0>,< 2.00, 1.20, 1.00>   
                material {RumpfTextur}              
                scale <1,1,1> rotate<0,0,0> translate<0,0.4,-0.5> 
                } // end of box --------------------------------------

         }         //Ende von Difference     
             
         //AchtungBand
         /*
         difference
         {             
            cylinder { <0,0,0>,<0,.10,0>, 1.50 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
            cylinder { <0,-0.1,0>,<0,.20,0>, 1.40 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
         }//Ende von Difference
                               */             
         //Luke
         //Luke �ffnen 
         #if((clock>0.2)&(clock<=0.4))
            object { KapselLuke_eigen rotate<0,-90,25> translate<1.15,1.1,0> } //Luke zu
         #end
         #if((clock>0.4)&(clock<=1))                                              //Luke geht auf
            object { KapselLuke_eigen translate<-1.270*0.5,0,-0.12> rotate<0,-90*(clock-0.4)/0.6,0> translate<-1.270*0.5,0,-0.12>*(-1) rotate<0,-90,25> translate<1.15-0.2*(clock-0.4)/0.6,1.1,0> }
         #end
         //#if(clock>0.6)   
         //   object { KapselLuke translate<-0.16,0,0> rotate<0,-180,0> translate<0.16,0,0.75> rotate<0,0,25> translate<1.15,1.1,0> }
         //#end                                                                                  
                                                                                          
         //Konstruktion der Raketenspitze
                        
         //Verbindung                        
         cone { <0,2.5,0>,0.5,<0,2.6,0>,0.46 
                         texture { pigment{ color rgbf <.4,0.4,0.4,0> } 
                     normal { bumps 0.5 scale 0.25}
                     finish { phong 0.2 reflection 0.30}} 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------

         //Orangenschale
         cone { <0,2.6,0>,0.46,<0,3.4,0>,0.1 
                           texture { spiral1 5 scale 0.5                   //---------------- 
                   texture_map{ [0.25 pigment{ color rgbf <.4,0.4,0.4,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}]
                                [0.25 pigment{ color rgbf <1,1,1,1>} ]
                                [0.75 pigment{ color rgbf <1,1,1,1>} ]
                                [0.75 pigment{ color rgbf <.4,0.4,0.4,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}]
                              } // end of texture_map
                    rotate<90,0,0>
                 } // end of texture ------------------------------------- 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------           
            
            //Unterliegendes Metall
            cone { <0,2.6,0>,0.45,<0,3.4,0>,0.09 
                         texture{ pigment{ color rgbf <1,0.65,0.2,0>}
                                      normal { bumps 0.5 scale 0.25}
                                      finish { phong 0.2 reflection 0.30}}
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone ------------------------------------- 
                      
            //Spitze          
            cone { <0,3.4,0>,0.1,<0,3.5,0>,0.04 
                         texture { pigment {color rgbf <.4,0.4,0.4,0>} 
                     normal { bumps 0.5 scale 0.25}
                     finish { phong 0.2 reflection 0.30}} 
                  scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
            } // end of cone -------------------------------------
            
        translate <0,-KapselHoehe/2,0> translate <0,untereStageHoehe + obereStageHoeheVerkuppelt + KapselHoehe/2,0> scale<1,1,1>*2 rotate<0,90,0> translate<0,-34,0>

    }//Ende von Union
#end   //Ende der Luken�ffnung 
 
    //Rechte Lampe
    object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>}
    object{Rotationsobjekt rotate<0,270+360*(clock),0> translate<0,SignallampeHoehe/2,0> scale 0.3 translate<11,LaunchpadHoehe*10/4,0>} 
    light_source{ <11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0> color rgb <1,0,0>*4   
              spotlight
              point_at<0,0,0>
              radius 1  // hotspot
              tightness 100
              falloff 60
              translate<0,LaunchpadHoehe*10/4+1,0>
              translate<-11,0,0> rotate<0,270+360*(clock),0> translate<11,0,0> 
            }   
            
    cone {
        <11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0>,0,<-0,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2+2,0>,4
        pigment {rgbt <1,0,0,0.95>}
        interior {
                media {
                        emission 0.67/3 // intensity of air glow
                        density {
                                cylindrical
                                }
                }
        } 
    hollow
    translate<-11,0,0> rotate<0,270+360*(clock),0> translate<11,0,0>}       
    
    //Linke Lampe
    object{Lampengehaeuse translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}
    object{Rotationsobjekt rotate<0,270+360*(clock),0> translate<0,SignallampeHoehe/2,0> scale 0.3 translate<-11,LaunchpadHoehe*10/4,0>}
    light_source{ <-11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0> color rgb <1,0,0>*4   
              spotlight
              point_at<0,0,0>
              radius 1  // hotspot
              tightness 100
              falloff 60
              translate<0,LaunchpadHoehe*10/4+1,0>
              translate<11,0,0> rotate<0,270+360*(clock),0> translate<-11,0,0> 
            }   
            
    cone {
        <-11,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2,0>,0,<-0,LaunchpadHoehe*10/4+SignallampeHoehe*0.3/2+2,0>,4
        pigment {rgbt <1,0,0,0.95>}
        interior {
                media {
                        emission 0.67/3 // intensity of air glow
                        density {
                                cylindrical
                                }
                }
        } 
    hollow
    translate<11,0,0> rotate<0,270+360*(clock),0> translate<-11,0,0>}


//Signaltafel und Startknopf
object{Anzeigetafel scale 1 translate<0,AnzeigetafelHoehe/2,0> scale 1 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-22>}
object{Startschalter scale 0.7 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4,-20>}
object{Startknopf scale 0.7 rotate<0,90,0> translate<0,LaunchpadHoehe*10/4-StartknopfHoehe*0.98,-20>} 

#declare Active_Texture = 
//    texture { pigment{ color rgb< 1, 0.0, 0>*1.2 } //  color Red
//    texture { pigment{ color rgb< 0.25, 0.5, 0>*1.7  } //  color Green
  texture { pigment{ color rgb< 1, 1, 1>*1.1  } //  color White
                   finish { ambient 0.9 diffuse 0.1 phong 1}
              } // end of texture 
#declare Inactive_Texture = 
    texture { pigment{ color rgb< 1, 1, 1>*0.35 } //  color gray
                   finish { phong 1 reflection 0.00}
              } // end of texture 
#declare Background_Texture =
    texture { pigment{ color rgb< 1, 1, 1>*0.05 } //  color nearly black
                   finish { phong 1 reflection 0.0}
              } // end of texture
              
#local Number = 26- int(clock*4); //0~99

#local Num_10 = int ( Number/10);
#local Num_1  = int (mod(Number,10));

#if(Num_10=0) #declare Num_10=99; #end                 
                                          //4 Sekunden = 200 Bilder
union
{
object{ Seven_Segment_LCD(          
         Num_1, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<0.88+0.42,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------                                               
       
object{ Seven_Segment_LCD(          
         Num_10, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<0.44,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------

object{ Seven_Segment_LCD(          
         0, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         1, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<-0.44,LaunchpadHoehe*10/4,-22.1>
       } // ------------------------           
       
object{ Seven_Segment_LCD(          
         0, // 0~9, integer!
         10, // shearing angle
         < 1.75, 10, 1.40>, //
         Active_Texture,  
         Inactive_Texture,  
         Background_Texture,  
         0, // SS_Point_On, 
         1, // SS_Point_Active, 
         ) //-----------------------
         scale 0.08
         rotate<-90,0,0> translate<0,AnzeigetafelHoehe*1.5,0> scale 1 translate<-(0.88+0.42),LaunchpadHoehe*10/4,-22.1>
       } // ------------------------
translate<0,0,22.1> rotate<0,180,0> translate<0,0,-22.1>
}//Ende union       
                 
          
//Typ                                                  
union{ 

object{Kopf rotate<0,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<0,0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
   
object{Oberkoerper translate<0,BeinHoehe,0> scale Typfaktor }  

#if(clock<=0.2)
    object{Arm rotate <0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }//linker Arm
    object{Arm rotate <0,80+10*clock/0.2,0> rotate<-90-90*clock/0.2,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor } //rechter Arm 
#end
#if((clock>0.2) & (clock<=0.4))
    object{Arm rotate <0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }//linker Arm
    object{Arm rotate <0,90,0> rotate<-180+90*(clock-0.2)/0.2,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor } //rechter Arm
#end 
#if((clock>0.4))
    object{Arm rotate <0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }//linker Arm
    object{Arm rotate <0,90,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor } //rechter Arm
#end

#if(clock<=0.4)
    object{Bein rotate<0,0,0> translate<UnterkoerperBreite/2-0.4,BeinHoehe,0> scale Typfaktor }
    object{Bein rotate<0,0,0> translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0> scale Typfaktor }
#else
    object{Bein rotate<-30*sin(clock*2*pi),0,0> translate<UnterkoerperBreite/2-0.4,BeinHoehe,0> scale Typfaktor }
    object{Bein rotate<30*sin(clock*2*pi),0,0> translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0> scale Typfaktor }
#end
    
 rotate<0,180,0> 
 #if(clock<=0.4)
    translate<0,LaunchpadHoehe*10/4,-3.75>
 #else                                      
    translate<0,LaunchpadHoehe*10/4,-3.75-1.2*(clock-0.4)/0.6>
 #end
}        

//Hammer  
#if(clock<=0.4)
    object{Hammer translate<Armlaenge,0,0> rotate<0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor rotate<0,180,0> translate<0,LaunchpadHoehe*10/4,-3.75>}                
 #else                                      
    object{Hammer translate<Armlaenge,0,0> rotate<0,80,0> rotate<-90,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor rotate<0,180,0> translate<0,LaunchpadHoehe*10/4,-3.75-1.2*(clock-0.4)/0.6>}                
 #end


//Umgebung 
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,90,0>}
object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,-90,0>}


object{Berg scale 3 rotate<0,-90,0> translate<0,-10,0> scale <2,3,4> translate<-200,0,-10>}      

//object{Sonne}









