//#include "hintergrund.pov"
#include "katapultRad.pov"

#declare KatapultHolzTexturTurbulence =
    texture
    {
     pigment {  P_WoodGrain8A color_map { M_Wood7A } scale <1,5,1>
     warp{repeat 0.5*y}
     warp{turbulence 0.1}}                  
    } 
    
#declare KatapultHolzTexturTurbulenceDunkel =
    texture
    {
     pigment { P_WoodGrain10A color_map { M_Wood8A }
     warp{repeat 0.5*y}
     warp{turbulence 0.1}}                  
    }

#declare KatapultBalken =
    union
    {
     box { <-0.20, 0.00, -3.00>,< 0.20, 0.800, 3.00>   

      texture { KatapultHolzTexturTurbulence scale 2 rotate<0,0,45> } // end of texture

      translate<0,-0.4,0> scale <0.5,0.5,1> rotate<0,0,0>  
    } // end of box --------------------------------------
    }//Ende Union
    
#declare KatapultZylinder =
    union
    {
        cylinder { <0,0,0>,<1.5,0,0>, 0.150
           texture { KatapultHolzTexturTurbulence scale 2 rotate<0,0,45> } // end of texture
           translate<0,0,0> scale <1,1,1> rotate<0,0,0>  
         } // end of cylinder  ------------------------------------

    }//Ende Union         
                  
#local KatapultHaelfteBreite = 1.32; 

#declare HebelX = KatapultHaelfteBreite - 0.32;
#declare HebelZ = 2+0.3;
#declare KatapultHoehe = 3.4; 
#declare KatapultMittelstangeHoehe = 0.65-0.15;

#declare KatapultRahmenHaelfte =
    union
    {                                                          
        //Aussenrahmen
        object{KatapultBalken translate <0,0.65,0>}
        object{KatapultBalken scale<1,1,0.5> rotate<90,0,0> translate<-0.1,2,0> }  
        object{KatapultBalken scale<0.75,1,0.6> rotate<45+90,0,0> translate<-0.1,1.75,-1.20>  }
        object{KatapultBalken scale<0.75,1,0.6> rotate<45+90,180,0> translate<-0.1,1.75,1.20>  }
                      
        //Raeder
        object{KatapultRad scale 0.5 translate<0.2,0.65,2>}
        object{KatapultRad scale 0.5 translate<0.2,0.65,-2>}
        
        //Verbindungssgtreben
        object{KatapultZylinder translate<-1-KatapultHaelfteBreite/2,0.65,0>}
        object{KatapultBalken scale<1.5,0.75,0.28> rotate<0,90,0> rotate <45,0,0> translate<-1+0.18,0.65,2+0.3>}
        object{KatapultBalken scale<1.5,0.75,0.28> rotate<0,90,0> rotate <45,0,0> translate<-1+0.18,0.65,-2-0.3>}
        object{KatapultBalken scale<1.8,0.5,0.28> rotate<0,90,0> translate<-1+0.18,3+0.3,0>}
        translate<1,-0.15-KatapultHoehe/2,0>
    }
     
#declare KatapultRahmen = 
    union
    {     
        object{KatapultRahmenHaelfte translate<KatapultHaelfteBreite/2,0,0> }
        object{KatapultRahmenHaelfte translate<KatapultHaelfteBreite/2,0,0> scale<-1,1,1>}
    }
    
#declare Schale =
    difference
    {
     sphere { <0,0,0>, 1 
            texture {KatapultHolzTexturTurbulenceDunkel}
          scale<1,1,1>  rotate<0,0,0>  translate<0,0.0,0>  
       }  // end of sphere -----------------------------------  
       
     sphere {<0,0,0>, 0.8 
            texture {KatapultHolzTexturTurbulence}
          scale<1,1,1>  rotate<0,0,0>  translate<0,0.0,0>  
       }  // end of sphere -----------------------------------
       
       box { <-3.00, 0.00, -3.00>,< 3.00, 3.00, 3.00>   

      texture { KatapultHolzTexturTurbulenceDunkel } // end of texture          
      scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
    } // end of box --------------------------------------
 
    }                     

#declare Loeffel =
    union                      
    {                          
        //Verbindungsstange
        cylinder { <-2,0,0>,<2,0,0>, 0.30
           texture { KatapultHolzTexturTurbulenceDunkel rotate<0,90,0> scale 0.5}
           scale <1,1,1> rotate<0,90,0> translate<0,0,0>
         } // end of cylinder  ------------------------------------
                           
         //Rotationszentrum
         cylinder { <-1,0,0>,<1,0,0>, 0.30
           texture { KatapultHolzTexturTurbulenceDunkel rotate<0,90,0> scale 0.5}
           scale <1,1,1> rotate<0,0,0> translate<0,0,-2+0.15>
         } // end of cylinder  ------------------------------------ 
         
         object{Schale translate<0,0.5,2.7>} 
         
         //Verschiebung des Rotationszentrum in den Nullpunkt
         translate<0,0,2-0.15> scale 0.75
   
    }                                
    
#declare Hebel =    
    union
    {
    //Verbindungsstange
        cylinder { <-1,0,0>,<2,0,0>, 0.10
           texture { KatapultHolzTexturTurbulenceDunkel rotate<0,90,0> scale 0.5}
           scale <1,1,1> rotate<0,90,0> translate<0,0,0>
         } // end of cylinder  ------------------------------------
                           
         //Rotationszentrum
         cylinder { <-0.5,0,0>,<0.5,0,0>, 0.30
           texture { KatapultHolzTexturTurbulenceDunkel rotate<0,90,0> scale 0.5}
           scale <1,1,1> rotate<0,0,0> translate<0,0,-2+0.15>
         } // end of cylinder  ------------------------------------  
         
         //Verschiebung des Rotationszentrum in den Nullpunkt
         translate<0,0,2-0.15> scale 0.75
      }
      
//object{KatapultRahmen translate<0,KatapultHoehe/2,0>}
//object{ Loeffel rotate <-15,0,0> translate<0,KatapultMittelstangeHoehe,0>}
//object {Hebel rotate <-90,0,0> translate<HebelX,KatapultMittelstangeHoehe,HebelZ>}
//object{KatapultRahmenHaelfte translate<KatapultHaelfteBreite/2,0,0> translate<0,KatapultHoehe/2,0>}
//object{KatapultZylinder}