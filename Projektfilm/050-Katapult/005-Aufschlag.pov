// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 

#include "Katapult.pov" 
#include "Hammer.pov"
#include "Typ.pov"
#include "Wald.pov"
#include "Berg_gross.pov"

//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {angle 75      // front view
                            location  <0.0 , 10.0 ,-10.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , 0.0>}
#declare Camera_1 = camera { angle 90   // diagonal view
                            location  <4.0 , 0.1 ,-0.0>
                            right     x*image_width/image_height
                            look_at   <4.0 , 0.0 , 1.0>}
#declare Camera_2 = camera {angle 90 // right side view
                            location  <10.0 , 10.0 , -1.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 5.0 , -1.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <0.0 , 10.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_4 = camera { angle 90        // top view
                            location  <30.0 , 10.0 ,-10.00>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}    
#declare Camera_5 = camera { angle 90        // top view
                            location  <150.0 , 70.0 ,-150.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}   
#declare Camera_10 = camera {angle 90      // front view
                            location  < -150, 30 ,-1>
                            right     x*image_width/image_height
                            look_at   <-100-100*clock,30+10*sin(clock*pi),0>}                                                                                 

camera{Camera_10}
// sun ---------------------------------------------------------------------
light_source{<-1500,2000,-2500> color White}

// sky -------------------------------------------------------------- 
plane{<0,1,0>,1 hollow  
       texture{ pigment{ bozo turbulence 0.92
                         color_map { [0.00 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.50 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.70 rgb <1,1,1>]
                                     [0.85 rgb <0.25,0.25,0.25>]
                                     [1.0 rgb <0.5,0.5,0.5>]}
                        scale<1,1,1.5>*2.5  translate< 0,0,0>
                       }
                finish {ambient 1 diffuse 0} }      
       scale 10000}
// fog on the ground -------------------------------------------------
fog { fog_type   2
      distance   50
      color      White  
      fog_offset 0.1
      fog_alt    1.5
      turbulence 1.8
    }

// ground ------------------------------------------------------------
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.72 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane
     
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------  

#declare Typfaktor =0.5;
          
///Typ                                                  
union{
object{Kopf rotate<0,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<0,0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
object{Oberkoerper translate<0,BeinHoehe,0> scale Typfaktor }  

object{Arm rotate <0,0,-60> rotate<-60,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor } 
object{Arm rotate <0,0,-60> rotate<60,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor }

object{Bein rotate<-50,0,20> translate<UnterkoerperBreite/2-0.4,BeinHoehe,0> scale Typfaktor }
object{Bein rotate<-50,0,-20> translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0> scale Typfaktor }
 rotate<30,90,0> translate<-100-100*clock,30+10*sin(clock*pi),0>
}        

//Hammer
object{Hammer rotate<90,0,0> translate<0,0.1,0>  scale Typfaktor rotate<0,70,0> translate<3,0,1>}  

//Katapult
union
{
object{KatapultRahmen translate<0,KatapultHoehe/2,0> scale 1 } 
object{ Loeffel rotate <-15-30,0,0> translate<0,KatapultMittelstangeHoehe,0> scale 1 }   
object {Hebel rotate <-110,0,0> translate<HebelX,KatapultMittelstangeHoehe,HebelZ> scale 1 }
     
rotate<0,90,0>
}

//Umgebung                                                      
union
{
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,0,0> scale 3 translate<-40,0,40> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<50,0,0> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,0,0> scale 3 translate<0,0,-120> rotate<0,90,0>}
    translate<0,0,0>
}    

object{Berg scale 3 rotate<0,-90,0> translate<0,-10,0> scale <2,3,4> translate<-180,0,-100>}         
 












