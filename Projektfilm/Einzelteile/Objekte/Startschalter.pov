/*#include "hintergrund.pov"

camera{Camera_1}       
  */
#declare StartschalterHoehe = 0.4+2+2;
#declare StartknopfHoehe = 0.2;

#declare Startschalter =  
    difference
    {
    union
    {
        //Schaltplatte
        box 
        { <-1.00, 0.00, -1.00>,< 1.00, 0.400, 1.00>   
         texture { 
                pigment{ image_map{ jpeg "zielscheibe.jpg" map_type 0} 
                scale 2 rotate<90,0,0> translate<-1,0,-1>}
                 } // end of texture           
         scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
        } // end of box --------------------------------------
                              
        //Stützsaeule
        box 
        { <-0.250, -4.00, -0.2500>,< 0.2500, 0.00, 0.2500>   
            texture { 
                pigment{ image_map{ jpeg "MetalPainted0164_2_S.jpg" map_type 0 interpolate 2} 
                scale 1 rotate<0,45,0> translate<-1,0,-1>}
                 } // end of texture             
            scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
        } // end of box --------------------------------------
        
        //Bodenverankerung 
          
        #for(index, 0, 4)
        
            box 
            { <-0.20, -3.00, -0.200>,< 0.200, 0.00, 0.200>   

                texture { 
                pigment{ image_map{ jpeg "MetalPainted0164_2_S.jpg" map_type 0 interpolate 2} 
                scale 1 rotate<0,0,0> translate<-1,0,-1>}
                 } // end of texture   

                scale <1,1,1> rotate<30,90*index,0> translate<0,-2,0> 
            } // end of box --------------------------------------
        #end //for-loop
        
        
     } //Ende von union  
         box 
        { <-2, -4.00, -2>,< 2, -5.00, 2>   
                     
            scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
        } // end of box --------------------------------------
        translate<0,1.8,0>  
     }//Ende von Difference 
      
     
#declare Startknopf =
     cylinder 
        { <0,-0.1,0>,<0,0.10,0>, 0.30  
          texture 
          { pigment{ color rgb< 0.5, 0.0, 0.0> } //   dark red  
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 reflection 0.00}
          } // end of texture         
           scale <1,1,1> rotate<0,0,0> translate<0,StartknopfHoehe/2+StartschalterHoehe/2,0>
         } // end of cylinder -------------------------------------
         
/*object{Startschalter translate<0,0,0>}
object{Startknopf }*/