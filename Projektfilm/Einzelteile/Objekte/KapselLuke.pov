// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 
#include "Ventilrad.pov"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 75      // front view
                            location  <0.0 , 0.3 ,-2.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 0.3 , 0.0>}
#declare Camera_1 = camera { angle 90   // diagonal view
                            location  <0.0 , 0 ,-2>
                            right     x*image_width/image_height
                            look_at   <0.0 , 0.0 , 0.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera {angle 90        // top view
                            location  <0.0 , 3.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_1}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>} 
          
           } //end of skysphere */
// ground ------------------------------------------------------------------
/*
plane{ <0,1,0>, 0 
       texture{ pigment{ color rgb <0.7,0.5,0.3>}
              //normal { bumps 0.75 scale 0.025}
                finish { phong 0.1}
              } // end of texture
     } // end of plane
*/
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------

//-------------------------------------------------------------------
            
#declare LukeAussenfarbe =
    material
    {
        texture { pigment { color rgb<1,1,1> }
                   //normal  { bumps 0.5 scale <0.005,0.25,0.005>}
                     finish  { phong 0.5 reflection{ 0.00 metallic 0.00} } 
                   } // end of texture
    }
    
#declare LukeInnenfarbe =
    material
    {
        texture { pigment { color rgb<1,1,1>*3 }
                   
                   } // end of texture
    }

#declare LukeGlas =
   material{ texture { NBglass } // end of texture 
     interior{ I_Glass } // end of interior
     } // end of material -------------------
     
#declare LukeGlas2 =
    material{ texture { NBglass } // end of texture 
        interior{ I_Glass } // end of interior
        } // end of material ------------------- 
        
#declare LukeGlasRing =
             texture { pigment{ color rgb< 1, 1, 1>*0.00 } //  color Black
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 }
                 } // end of texture 



    
#declare KapselLukeZylinder =
    union
    {          
        difference
        {
            cylinder { <0,-.75,0>,<0,.7500,0>, 1.50 
                material{LukeAussenfarbe}         

                scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
            cylinder { <0,-1,0>,<0,1.0,0>, 1.40 
                material{LukeInnenfarbe}
                scale <1,1,1> rotate<0,0,0> translate<0,0,0>
            } // end of cylinder -------------------------------------
            
           cylinder { <0,0,-3>,<0,0,3.00>,0.150 
            material{LukeGlas}
            scale <1,1,1> rotate<0,90+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------

         }//Ende von Difference
         cylinder { <0,0,1.37>,<0,0,1.520>,0.150 
            material{LukeGlas2}
            scale <1,1,1> rotate<0,90+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------
         difference
         {
            cylinder { <-1.55,0,0>,<-1.3,0,0>, 0.20
                 texture { LukeGlasRing } // end of texture
                scale <1,1,1> rotate<0,180+45/2,0> translate<0,0.3,0>
                } // end of cylinder  ------------------------------------
                
            cylinder { <-1.6,0,0>,<1.1,0,0>,0.1510 
            texture{LukeGlasRing}
            scale <1,1,1> rotate<0,180+45/2,0> translate<0,0.3,0>
         } // end of cylinder  ------------------------------------    
         }//Ende von Difference
        
        object{Ventilrad
        
        scale 0.15 rotate<-90,0,0> translate<0,-0.2,-1.65> rotate<0,-90+45/2,0> }   

    }//Ende von union

//-------------------------------------------------------------------
#include "shapes3.inc"
//-------------------------------------------------------------------
#declare KapselLuke =
    object{ Segment_of_Object( KapselLukeZylinder, 45 )   
        texture{ pigment{ color rgb<1,1,1>} 
                 finish { phong 1}
               } // end of texture 
        scale <1,1,1> rotate<0,45+45/2,0> translate<0,0,1.5>
      }
      
//-------------------------------------------------------------------
 
  
   //object{KapselLuke}