/*#include "hintergrund.pov"
                            

camera{Camera_1} */
                     
#declare AxtHoehe = 2.090+0.2+0.2;
                     
#declare Kopf_Textur1 =
             texture{ Chrome_Metal
                  // normal { bumps 0.5 scale 0.05 } 
                  // finish { phong 1 }
                } // end of texture ---------------------------  
       
     
#declare Kopf_Textur2 = 
                     texture { T_Grnt15
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 0.5 
                 } // end of texture 



#declare Axt =
union{                 
      cylinder { <0,0,0>,<0,2.00,0>, 0.10 

           texture { T_Wood11 rotate 0 scale 4
                     pigment{
                        turbulence 10
                        }
                   } // end of texture

           scale <1.25,1,1> rotate<0,0,0> translate<0,0,0>
       } // end of cylinder ------------------------------------- 
                 
       //Axtkopf  
       box
       {
            <-0.0,-0.1,-0.11> <0.6,0.2,0.11> 
            translate <-0.2,2.09,0>
            texture{Kopf_Textur2}
       }        
       difference
       {
       // linear prism in y-direction: from .. ,to ..,number of points (first = last)
        prism { 2.0900-0.1-0.2 ,2.090+0.2+0.2 , 5
       <-0.2, -0.11>, // first point 
       < -0.200, 0.11>,   
       <-0.4,0.05>,
       < -0.40,-0.05>,
       <-0.20, -0.11>  // last point = first point !!!

       texture {Kopf_Textur2} // end of texture

       //scale 0.25
       //rotate<0,90,0> 
       translate<0.00, 0.00, 0.00> 
     } // end of prism --------------------------------------------------------    
     
        sphere { <0,0,0>, 1 

          texture {Kopf_Textur2} // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,3.27,0>  
       }  // end of sphere -----------------------------------
       
       sphere { <0,0,0>, 1 

               texture {Kopf_Textur2} // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,1.01,0>  
       }  // end of sphere -----------------------------------  


     } //Ende von Difference
       
     difference
     {
    // linear prism in y-direction: from .. ,to ..,number of points (first = last)
        prism { 2.0900-0.1-0.2 ,2.090+0.2+0.2 , 4
       <-0.2, -0.05>, // first point 
       < -0.200, 0.05>,   
       <-0.4,0.0>,       
       <-0.20, -0.05>  // last point = first point !!!

       texture {Kopf_Textur1} // end of texture

       //scale 0.25
       //rotate<0,90,0> 
       translate<-0.20, 0.00, 0.00> 
     } // end of prism --------------------------------------------------------     
     
     sphere { <0,0,0>, 1 

               texture {Kopf_Textur1} // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,3.27,0>  
       }  // end of sphere -----------------------------------
       
       sphere { <0,0,0>, 1 

               texture {Kopf_Textur1} // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,1.01,0>  
       }  // end of sphere -----------------------------------  
       } //Ende von Difference     
       translate<0,-AxtHoehe/2,0>

}//Ende der Vereinigung       

//object{Axt}