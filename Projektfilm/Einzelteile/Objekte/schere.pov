// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {/*ultra_wide_angle*/ angle 75      // front view
                            location  <0.0 , 0.0 ,-6.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 0.0 , 0.0>}
#declare Camera_1 = camera {/*ultra_wide_angle*/ angle 90   // diagonal view
                            location  <2.0 , 2.5 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_2 = camera {/*ultra_wide_angle*/ angle 90  //right side view
                            location  <5.0 , 0.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 0.0 , 0.0>}
#declare Camera_3 = camera {/*ultra_wide_angle*/ angle 90        // top view
                            location  <0.0 , 10.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 0.0 , 0.0>}
camera{Camera_3}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>} 
          
           } //end of skysphere
// ground ------------------------------------------------------------------
/*
plane{ <0,1,0>, 0 
       texture{ pigment{ color rgb <0.7,0.5,0.3>}
              //normal { bumps 0.75 scale 0.025}
                finish { phong 0.1}
              } // end of texture
     } // end of plane
*/
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
#declare griff_Textur =
             texture { pigment{ color rgb< 0.5, 0.0, 0.0> } //   dark red  
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 reflection 0.00}
                 } // end of texture 

#declare schneide_Textur =
             texture{ Chrome_Metal
                  // normal { bumps 0.5 scale 0.05 } 
                  // finish { phong 1 }
                } // end of texture ---------------------------  

    
#declare scherenblatt =

    union
    {         
      difference
      {
      torus { 1.0,0.25  rotate<0,0,0>
        texture { 
                    griff_Textur
                } // end of texture
        scale <1,0.75,0.8> rotate<0,0,0>  translate<0,0,0.2>
      } // end of torus  -------------------------------
      box{<2,.15,-2><-2,.35,2> texture{griff_Textur}} 
      box{<2,-.15,-2><-2,-.35,2> texture{griff_Textur}}    
      } //Ende von Difference
      
      box { <-.50, -0.15, -0.15>,< 2.0, .15, 0.3>                      

      texture { griff_Textur
              } // end of texture

      scale <1,1,1> rotate<0,0,0> translate<0,0,-0.55> 
    } // end of box --------------------------------------
    
    // linear prism in x-direction: from ... to ..., number of points (first = last) 
    difference
    {
    prism { -1.00 ,6.00 , 5
        <-1.00, 0.00>, // first point
        < 1.00, 0.00>, 
        < 1.00, .10>,
        <-1.00, .25>,
        <-1.00, 0.00>  // last point = first point!!!
        rotate<-90,-90,0> //turns prism in x direction! Don't change this line!  

      texture { schneide_Textur
              } // end of texture

       scale <1.00, .250, .60>
       rotate<0,0,0> 
       translate<2.900, 0.00, -.7500> 
     } // end of prism --------------------------------------------------------
     box { <-1.200, -1.00, -1.00>,< 5.00, 1.00, 1.00>   
                            texture { schneide_Textur 
              } // end of texture

      scale <1,1,1> rotate<0,5,0> translate<2.9,0,-2> 
      } //End von Box 
      
      box { <-1.200, -1.00, -1.00>,< 7.00, 1.00, 1.00>   
                            texture { schneide_Textur 
              } // end of texture

      scale <1,1,1> rotate<0,-7,0> translate<2.9,0,-2> 
    } // end of box --------------------------------------
  
     
     } //Ende von Difference
      
           
  
    }   
    
    //---------------------------------------------------------------------------- 
#include "shapes3.inc"
//----------------------------------------------------------------------------
                                                    
#declare schere =
union{
    object{scherenblatt rotate <0,10,0> translate <0.5,0.1,1.1>}  
    object{scherenblatt scale <1,1,-1> rotate <0,-10,0> translate <.5,0,-1.1>}
    cylinder { <0,0,0>,<0,0.5,0>, 0.30 

           texture { schneide_Textur 
                   } // end of texture

           scale 0.75 rotate<0,0,0> translate<3,-0.1,0>
         } // end of cylinder -------------------------------------
         }
    object{schere}