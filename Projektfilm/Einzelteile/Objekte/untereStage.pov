// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 75      // front view
                            location  <0.0 , 8.0 ,-20.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 8.0 , 0.0>}
#declare Camera_1 = camera {angle 90   // schraege view
                            location  <-1.0 , 16 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 13 , 1.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <3.0 , 16.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 14.0 , 0.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <-1.0 , 20.0 ,1>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 1.0 , 1.0>}
#declare Camera_4 = camera { angle 90        // bottom view
                            location  <1.0 , -3 ,-1>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 0.0 , 1.0>}
camera{Camera_4}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>} 
          
           } //end of skysphere          */
// ground ------------------------------------------------------------------
/*
plane{ <0,1,0>, 0 
       texture{ pigment{ color rgb <0.7,0.5,0.3>}
              //normal { bumps 0.75 scale 0.025}
                finish { phong 0.1}
              } // end of texture
     } // end of plane
*/
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------    

#declare untereStageStiftHoehe = 0.25;

#declare untereStageHoehe = 1.5+13.25-untereStageStiftHoehe;  

#include "Triebwerk.pov" 

#declare Aussenhaut =
    texture{ pigment{ color rgb<0.85,0.85,0.80> }
                 normal { radial poly_wave frequency 12 scale 1.17  turbulence 0 rotate<90,0,0> }
                 finish { phong 1 phong_size 250 reflection{ 0.01} }
               } // end of texture --------------------------------------------------------------------------------------------


#declare Aussenhaut2 =
        texture{ pigment{ color rgb<0.85,0.95,0.80> }
                 normal { radial poly_wave frequency 12 scale 1.17  turbulence 0 rotate<90,0,0> }
                 finish { phong 1 phong_size 250 reflection{ 0.01} }
               } // end of texture --------------------------------------------------------------------------------------------

#declare Aussenhaut3 =
              texture{ pigment{ color rgb< 1, 1, 1>*0.65 } //  color Gray75
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 phong_size 250 reflection{0.01} }
                 } // end of texture 


#declare Aussenhaut4 = 
          texture{ pigment{ color rgb< 1, 1, 1>*0.85 } //  color Gray85
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 1 phong_size 250 reflection{0.01} }
                 } // end of texture    
                 
#declare Aussenhaut5 = 
                    texture { T_Silver_3A
                   //normal { bumps 0.5 scale 0.15} 
                   finish { phong 1}
                 } // end of texture 

#local AchtungBandTextur =
              texture{ pigment{ gradient <1,1,0>
                            color_map{
                                [ 0.0 color rgb<1,1,1> ]
                                [ 0.3 color rgb<1,1,1> ]
                                [ 0.3 color rgb<1,0,0>*0.3 ]
                                [ 1.0 color rgb<1,0,0>*0.7 ]
                               } // end color_map
                             scale 0.3
                           } // end pigment
                 //normal  { bumps 0.5 scale  0.005 }
                   finish  { phong 1 reflection 0.00 }
                 } // end of texture -------------------


     
#declare Wing =
union{
 cone  { <0,0,0>, 2.40,<0,9.50,0>, 1.00}
 sphere{ <0,0,0>, 1.00 scale<1,0.15,1> translate<0,9.50,0>} 
 scale <1,1,0.15>
 matrix<     1, 0, 0, //Shearing in X direction
          0.10, 1, 0,
             0, 0, 1,
             0, 0, 0>    
  texture{pigment{color rgb<1,1,1>*0.95}}
  }//Quelle: http://www.f-lohmueller.de/pov_tut/x_sam/tec_805d.htm [Zuletzt aufgerufen: 14.12.2014]
  
  
#declare Booster =
    union
    {    
    difference
    {
        cone{ <0,0,0>,1.00,<0,2,0>,0.950 
                texture{ Aussenhaut5 //scale <1,1,1>
             } // end of texture
            scale <1,1,1> rotate<0,0,0> translate<0,0.0,0> 
    } // end of cone -----------------------------------
    
    cylinder { <0,-1,0>,<0,1.00,0>, 0.90 

           texture { Aussenhaut3
                   } // end of texture
         } // end of cylinder ------------------------------------- 
        
    }//End von Difference
    
    cone{ <0,2,0>,1.00,<0,4,0>,1 

      texture{ Aussenhaut4 //scale <1,1,1>
             } // end of texture
      scale <1,1,1> rotate<0,0,0> translate<0,0.0,0> 
    } // end of cone -----------------------------------
    
    cone{ <0,4,0>,0.9,<0,5,0>,0.9 

      texture{ Aussenhaut5 //scale <1,1,1>
             } // end of texture
      scale <1,1,1> rotate<0,0,0> translate<0,0.0,0> 
    } // end of cone -----------------------------------
    
    cone{ <0,5,0>,0.950,<0,10,0>,0.70 

      texture{ Aussenhaut4 //scale <1,1,1>
             } // end of texture
      scale <1,1,1> rotate<0,0,0> translate<0,0.0,0> 
    } // end of cone -----------------------------------   
    
    cone { <0,10,0>,0.7,<0,12,0>,0.1 

       texture { Aussenhaut4 } // end of texture

       scale <1,1,1> rotate<0,0,0> translate<0,0,0>         
     } // end of cone -------------------------------------

    object{ Wing rotate<0,0,-90> scale <0.2,0.3,0.3> translate <1,3,0>}
    // linear prism in x-direction: from ... to ..., number of points (first = last)
    prism { -0.05 ,0.050 , 5
        <-1.00, 0.00>, // first point
        < 1.00, 0.00>, 
        < 0.10, 1.50>,
        <-0.10, 1.50>,
        <-1.00, 0.00>  // last point = first point!!!
        rotate<0,0,0> //turns prism in x direction! Don't change this line!  

      texture { pigment{color rgb<1,1,1>*0.95}
              } // end of texture

       scale <1.00, 1.00, 1.00>*0.5
       rotate<0,90,90> 
       translate<2.95, 2.30, 0.00> 
     } // end of prism -------------------------------------------------------- 
     
     object{Triebwerk scale 0.75 translate<0,-1.5,0>}


    } 
    
#declare Antrieb =
    union
    {
        object{ Booster rotate<0,45,0>}
        object{ Booster rotate<0,135,0> translate <-2,0,0>}
        object{ Booster rotate<0,225,0> translate <-2,0,2>}
        object{ Booster rotate<0,-45,0> translate < 0,0,2>}
    }               
    
#declare UntererTank =
    union
    {
        difference
        {
            ovus{ 1.00, 0.75 // base_radius, top_radius  with: top_radius< base_radius! 
            texture{ pigment{ color rgb< 1.0, 0.9, 0.8>*0.9}  
                   finish { phong 0.3 reflection { 0.00 metallic 0.00}  }} // end of texture 
            scale <1.75,-5,1.75>
            rotate<0,0,0>
            translate<-1,12,1> 
            }
             
            cylinder { <0,0,0>,<0,9.00,0>, 0.30 

           texture { Chrome_Metal
                   //normal  { bumps 0.5 scale <0.005,0.25,0.005>}
                     finish  { phong 0.5 reflection{ 0.00 metallic 0.00} } 
                   } // end of texture

           scale <5,1,5> rotate<0,0,0> translate<-1,8,1>
             } // end of cylinder -------------------------------------
         
             cylinder { <0,0,0>,<0,10.00,0>, 0.30 

                    texture{ Chrome_Metal
                  // normal { bumps 0.5 scale 0.05 } 
                  // finish { phong 1 }
                } // end of texture ---------------------------  


               scale <7,1,7> rotate<0,0,0> translate<-1,13,1>
             } // end of cylinder -------------------------------------  
          }  //Ende von Difference
         //Verbindungslöcher einfügen
         #declare X = 0;  // start
         #declare End_X = 12; // end
         #while ( X <= End_X )   
            //object{Antrieb translate<0,5,0>}
             cylinder { <0,13,0>,<0,13.25,0>, 0.05 
                    texture{ Chrome_Metal }       
                    scale <1,1,1> rotate<0,0,0> 
                    translate<1.6,0,0> 
                    rotate<0,(X*(360/End_X)),0>
                    translate<-1,0,1>
                     } // end of cylinder ------------------------------------- 
            
            #declare X = X + 1; // next
          #end //-------------- end of loop ----
          
          //Tankdeckel einfuegen
          sphere { <0,0,0>, 1.5 

                texture{ pigment{ color rgb<1.00, 0.55, 0.00> }
                 normal { pigment_pattern{radial frequency 5 sine_wave
                                         color_map {[0.0, rgb 0]
                                                    [0.1, rgb 1]
                                                    [0.9, rgb 1]
                                                    [1.0, rgb 0]}
                                         rotate<0,0,0> scale 0.5} 1 }
                  finish { phong 1 phong_size 100  reflection{0.01} }
                } // end of texture ------------------------------------


          scale<1,0.5,1>  rotate<0,0,0>  translate<-1,12,1>  
       }  // end of sphere -----------------------------------   
       
       //AchtungBand
         difference
         {             
            cylinder { <0,13,0>,<0,13.10,0>, 1.750 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<-1,-0.12,1>
            } // end of cylinder -------------------------------------
            
            cylinder { <0,12.9,0>,<0,13.2,0>, 1.70 
            texture { AchtungBandTextur }
            scale <1,1,1> rotate<0,0,0> translate<-1,-0.12,1>
            } // end of cylinder -------------------------------------
         }//Ende von Difference



           
        //}
//-------------------------------------------------------------------------

    } //Ende Union von UntererTank
           
#declare untereStage =
    union
    {
        object{Antrieb}
        object{UntererTank} 
        translate <1,-untereStageHoehe/2+1.5,-1>
        
    }

//object{untereStage}