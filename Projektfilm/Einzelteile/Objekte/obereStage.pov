// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
#include "untereStage.pov"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
/*#declare Camera_0 = camera { angle 75      // front view
                            location  <0.0 , 16.0 ,-10.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 16.0 , 0.0>}
#declare Camera_1 = camera {angle 90   // schraege view
                            location  <-1.0 , 18 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 16 , 1.0>}
#declare Camera_2 = camera { angle 90  //right side view
                            location  <7.0 , 16.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 16.0 , 0.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <-1.0 , 30.0 ,1>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 1.0 , 1.0>}
#declare Camera_4 = camera { angle 90        // bottom view
                            location  <1.0 , 10.0 ,-1>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 12.0 , 1.0>}
#declare Camera_5 = camera { angle 90        // full view
                            location  <1.0 , 7.0 ,-15>
                            right     x*image_width/image_height
                            look_at   <-1.0 , 7.0 , 1.0>}                            
camera{Camera_0}
// sun ---------------------------------------------------------------------
light_source{< -3000, 3000, -3000> color White}
// sky ---------------------------------------------------------------------
sky_sphere { pigment { color rgb <1.0,1.0,1.0>*0.6} 
          
           } //end of skysphere   */
// ground ------------------------------------------------------------------
/*
plane{ <0,1,0>, 0 
       texture{ pigment{ color rgb <0.7,0.5,0.3>}
              //normal { bumps 0.75 scale 0.025}
                finish { phong 0.1}
              } // end of texture
     } // end of plane
*/
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------
#declare obereStageHoehe = 6; //inklusive Triebwerk
#declare obereStageHoeheVerkuppelt = 5;//abz�glich des Triebwerks

#declare Aussenhuelle =
    texture {
    pigment
        {
        gradient <0,1,0>         // specify gradient direction vector
  
            color_map
            {
            [0.0 color rgb< 1.0, 0.9, 0.8>*0.9] 
            [0.9 color rgb< 1.0, 0.9, 0.8>*0.9]
            /*[0.8 color rgb< 1.0, 0.9, 0.8>*0.8] 
            [1.0 color rgb< 1.0, 0.9, 0.8>*0.8]*/   
            /*[0.8 color rgb<0.75, 0.75, 1.0>] 
            [1.0 color rgb<0.75, 0.75, 1.0>]*/
            
            [0.9 color rgb< 1,1,1> ] 
            [1 color rgb<1,1,1>]           
            
            

            }// Ende Colormap
        } //Ende Pigment
        finish { phong 0 reflection 0.00} 
        rotate<0,0,90> scale 0.5}
    
#declare InnenHuelle =
    texture {pigment{color rgb<0.8,0.7,0.6>*0.5}}
    
#declare obereStage =
    union
    {       
        object{Triebwerk scale<1,1,1>*1 translate<-1,12,1>}
        
        difference
        {     
            cone { <0,0,0>,1.7,<0,2,0>,1.5 
                texture { Aussenhuelle }// end of texture
                scale <1,1,1> rotate<0,0,0> translate<-1,13,1>         
                } // end of cone -------------------------------------
                
            cone { <0,-1,0>,1.5,<0,2,0>,1.3 
                texture { InnenHuelle }// end of texture
                scale <1,1,1> rotate<0,0,0> translate<-1,13,1>         
                } // end of cone -------------------------------------
        }//Ende Difference 
        
        cone { <0,2,0>,1.5,<0,5,0>,1.5 
                texture { Aussenhuelle }// end of texture
                scale <1,1,1> rotate<0,0,0> translate<-1,13,1>         
                } // end of cone -------------------------------------
                
        object{ Wing rotate<0,0,-90> scale <0.2,0.3,0.3> 
                     matrix<1, -0.5, 0, // matrix-shear_x to y 
                            0,  1,  0,
                            0,  0,  1,
                            0,  0,  0>    
                     scale<0.5,1,1> rotate<0,0,5> translate <0.5,13.75,1> 
                }                   
        object{ Wing rotate<0,0,-90> scale <0.2,0.3,0.3> 
                     matrix<1, -0.5, 0, // matrix-shear_x to y 
                            0,  1,  0,
                            0,  0,  1,
                            0,  0,  0>    
                     scale<-0.5,1,1> rotate<0,0,-5> translate <-2.5,13.75,1> 
                }
         object{ Wing rotate<0,0,-90> scale <0.2,0.3,0.3> 
                     matrix<1, -0.5, 0, // matrix-shear_x to y 
                            0,  1,  0,
                            0,  0,  1,
                            0,  0,  0>    
                     scale<0.5,1,1> rotate<0,90,0> rotate<5,0,0> translate <-1,13.75,-0.5> 
                }
         object{ Wing rotate<0,0,-90> scale <0.2,0.3,0.3> 
                     matrix<1, -0.5, 0, // matrix-shear_x to y 
                            0,  1,  0,
                            0,  0,  1,
                            0,  0,  0>    
                     scale<-0.5,1,1> rotate<0,90,0> rotate<-5,0,0> translate <-1,13.75,2.5> 
                }
    translate<1,-12-obereStageHoehe/2,-1>
    }  //Ende Union
    
                 
                  
     
//object{untereStage}
//object{obereStage}
