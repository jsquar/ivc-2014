// PoVRay 3.7 Scene File " ... .pov"
// author:  Dreistein
// date:    11.12.2014
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {perspective angle 75               // front view
                            location  <0.0 , 2.0 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera {/*ultra_wide_angle*/ angle 90   // diagonal view
                            location  <2.0 , 2.5 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_2 = camera {/*ultra_wide_angle*/ angle 90  //right side view
                            location  <3.0 , 1.0 , 0.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_3 = camera {/*ultra_wide_angle*/ angle 90        // top view
                            location  <0.0 , 3.0 ,-0.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
camera{Camera_1}
// sun ----------------------------------------------------------------------
light_source{< 3000,3000,-3000> color White}

//---------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//---------------------------------------------------------------------------
#declare Kopf_Textur =
    texture
    {
         Brushed_Aluminum
         finish{diffuse 0.6 ambient 0.2}    
         rotate 45
         scale 0.5  
     } // end of texture ---------------------------              

#declare Hammer =
union{                 
      cylinder { <0,0,0>,<0,2.00,0>, 0.10 

           texture { T_Wood11 rotate 0 scale 4
                     pigment{
                        turbulence 10
                        }
                   } // end of texture

           scale <1.25,1,1> rotate<0,0,0> translate<0,0,0>
       } // end of cylinder ------------------------------------- 
       prism
       {
        -0.11, 0.11, 4, <0,-0.10>, <0,0.10>, <-0.30,-0.0>, <0,-0.10>            
        //rotate<0,90,0> 
        rotate<90,0,0>                                   
        
        translate<-0.20,2.09,0>        
        texture
        {
           Kopf_Textur
           
        }
       }//Ende des Prism
       box
       {
            <-0.0,-0.1,-0.11> <0.6,0.1,0.11> 
            translate <-0.2,2.09,0>
            texture{Kopf_Textur}
       }
}//Ende der Vereinigung      
object{Hammer}