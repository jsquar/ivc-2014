#include "hintergrund.pov"    
#include "shapes3.inc"

camera{Camera_1}
 
//Textur-Quelle: http://news.povray.org/povray.general/thread/%3C35d902ef.0@news.povray.org%3E/
#declare Concrete =
   texture { pigment { granite turbulence 1.5 color_map {
    [0  .25 color White color Gray75] [.25  .5 color White color Gray75]
    [.5 .75 color White color Gray75] [.75 1.1 color White color Gray75]}}
    finish { ambient 0.2 diffuse 0.3 crand 0.03 reflection 0 } normal {
    dents .5 scale .5 }}

   #declare Concrete1 =
   texture { pigment { granite turbulence 1.5 color_map {
    [0  .25 color White color Gray95] [.25  .5 color White color White]
    [.5 .75 color White color White] [.75 1.1 color White color Gray85]}}
    finish { ambient 0.2 diffuse 0.3 crand 0.003 reflection 0 } normal {
    dents .5 scale .5 }}

#declare Lochschneider =
    cylinder { <0,-4,0>,<0,4.00,0>, 1 
                    texture { T_Grnt9
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 1 
                 } // end of texture 

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------


#declare LaunchpadHoehe = 2;   
#declare LukeHoehe = 0.2; 
#declare LukenfachHoehe = 1.7;
#declare Launchpad =
    difference
    {
        //      Pyramid_N_AB( N,  Point_A, Radius_A,    Point_B, Radius_B )
        object{ Pyramid_N_AB( 8,<0,0,0>,6,<0,2.01,0>,3)
        texture{Concrete                               
               } // end of texture
                
        scale <1,1,1> rotate< 0,0, 0>  translate< 0, 0, 0>
        } // end of object -----------------------------------------------------
        //----------------------------------------------------------------------------  
        
        //Oberen Belag mit Platten auslegen
        box { <-5.00, 2.00, -5.00>,< 5.00, 2.20, 5.00>   

      texture { 
                pigment{ image_map{ jpeg "ConcretePlates0119_1_S.jpg" map_type 0} 
                scale 0.5 rotate<90,0,0> translate<-1,0,-1>}
                normal { bump_map{ jpeg "ConcretePlates0119_1_S_normal.jpg" map_type 0 bump_size 10}
                scale 0.5 rotate<90,0,0> translate<-1,0,-1>}
                 
                 
                 } // end of texture 
                scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
    } // end of box --------------------------------------

        
        //Loch reinschneiden 
        object{Lochschneider} 
        
        
        //Seitenf�cher f�r Luke reinschneiden
        cylinder { <0,0,0>,<0,LukeHoehe,0>, 2 

           texture {T_Grnt9
                   //normal { agate 0.15 scale 0.15}
                   finish { phong 0.5 } 
                   scale 1  } // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,LukenfachHoehe-LukeHoehe/2,0>
         } // end of cylinder -------------------------------------

         translate<0,-LaunchpadHoehe/2,0> 
     } //Ende von Difference  
               

#declare Luke =
        object{ Segment_of_CylinderRing( 1, // major radius,
                                 0.00, // minor radius,  
                                 LukeHoehe, // height H,
                                 180  // angle (in degrees)  
                               ) //-----------------------------------------
         texture{ Concrete1 } // end of texture 
            scale <1,1,1> rotate<0,0,0> translate<0,-LukeHoehe/2,0>
         } // end of object
            
            
    
    

object{Launchpad translate<0,-0.5,0,>}  
object{Luke translate<0,LaunchpadHoehe/2-0.5-LukeHoehe/2>}  
object{Luke translate<0,LaunchpadHoehe/2-0.5-LukeHoehe/2> rotate<0,180,0>}
       