#include "hintergrund.pov"

camera{Camera_0 translate<0,2,-2>} 

#include "Zylinder.pov"

#local HautFarbe =
             texture { pigment{ color rgb< 1.2, 0.75, 0.75>  } // very light red  
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 0.1 reflection 0.0}
                 } // end of texture 
                 
#local NaseFarbe =
    texture { pigment{ color rgb< 0.02, 0.02, 0.02>*0 } //  color Black
                // normal { bumps 0.5 scale 0.05 }                
                   finish { ambient color<0.1,0.1,0.1> ambient 0.5 phong 0.05 phong_size 10  }
           } // end of texture                     

#local ArmFarbe =
             texture { pigment{ color rgb< 0.5, 0.5, 1>*0.2 } //  color Gray10
                // normal { bumps 0.5 scale 0.05 }
                   finish { phong 0 reflection 0.00}
                 } // end of texture 

#local Auge = 
    union
    {
        sphere { <0,0,0>, 0.5 
                
        texture { pigment{ color rgb<1.00, 1.00, 1.00>}
                  finish { phong 1.0 reflection 0.10}                      
                } // end of texture    
                
        texture { 
                /*pigment{ image_map{ png "eyeball.png" map_type 1 once } 
                scale<1,1,1> rotate<0,0,0> rotate<0,0,0> translate<0,0,0>}*/
                pigment{
                 image_map{ png "eyeball.png"
                            map_type 0
                            once
                            
                          }
                 translate<-0.5,-0.5,0>
                 scale<1,1,1>*0.7
                 } // end pigment
                 } // end of texture               

          scale<1,1,1>  rotate<0,0,0> rotate<0,0,0>  translate<0,0,0>  
       }  // end of sphere ----------------------------------- 

  
    }     
    
#local Nase =
    union
    {
        sphere { <0,0,0>, 0.5 

        texture { NaseFarbe } // end of texture

          scale<1,1,1>  rotate<0,0,0>  translate<0,0,0>  
       }  // end of sphere ----------------------------------- 


    }

#declare OberkoerperHoehe = 2.5;
#declare OberkoerperBreite = 2;
#declare UnterkoerperBreite = 1.6;    
#declare Oberkoerper =      
    union
    {
        // linear prism in z-direction: from ,to ,number of points (first = last)
     prism { -2.00 ,2.00 , 7
       <-1.00, -3.00>,  // first point
       < 1.00, -3.00>, 
       < 0.90, 0.00>, 
       < 0.70, 1.0>, 
       <-0.70, 1.0>, 
       <-0.90, 0.00>, 
       <-1.00, -3.00>   // last point = first point!!!!
       rotate<-90,0,0> scale<1,1,-1> //turns prism in z direction! Don't change this line! 
       
       texture { pigment { image_map{ jpeg "Seamless-Aloha-Pattern.jpg"
                        map_type 1 interpolate 2  }
                        scale <1,1,1>*0.1 rotate 90}
                 finish { phong 1.0} 
               } // end of texture
       
       scale <1.00,1.00,1.00>       
       rotate <0,90,0> 
       translate <0.00,4,0.00> 
     } // end of prism --------------------------------------------------------     
     
     
     box { <-0.800, -0.500, -1.500>,< 0.800, 1.00, 1.500>   

      texture { pigment{
                 image_map{ jpeg "BuildingsVarious0042_S.jpg"
                            map_type 1
                          }        
                          translate<0,4,0>
                          scale 0.00
                          } //Ende von pigment
                 finish { phong 1.0} 
               } // end of texture

      scale <1,1,1> rotate<0,90,0> translate<0,0,0> 
    } // end of box --------------------------------------

     scale 0.5

    }//Ende union 

#declare ArmHoehe =  OberkoerperHoehe-0.5;
#declare Armlaenge = 2;    
#declare Arm =
    union
    {     
        cylinder { <0,0,0>,<0.75,0,0>, 0.30                 
       texture { pigment { image_map{ jpeg "Seamless-Aloha-Pattern.jpg"
                        map_type 2 interpolate 2  }
                        scale <4,1,1> rotate 90}
                 finish { phong 1.0} 
               } // end of texture
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder  ------------------------------------
    
        cylinder { <0,0,0>,<2,0,0>, 0.20
           texture { ArmFarbe } // end of texture
           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder  ------------------------------------
         
         sphere { <2,0,0>, 0.3 

        texture { HautFarbe} // end of texture

          scale<1,1,1>  rotate<0,0,0> translate<0,0,0>  
       }  // end of sphere -----------------------------------    
    }

#declare BeinHoehe = 2;    
#declare Bein =
    union
    {
        cylinder { <0,0.2,0>,<0,2.00,0>, 0.20 

           texture { ArmFarbe } // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------
         
         difference
         {
            sphere{ <0,0,0>, 0.35 
            texture { HautFarbe
                } // end of texture
            scale<2,0.5,1>  rotate<0,90,0>  translate<0,1,0>  
            } // end of sphere ----------------------------------- 
            
            box { <-1.00, 0.00, -1.00>,< 1.00, 1.00, 1.00>   
            texture { HautFarbe } // end of texture
            scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
            } // end of box --------------------------------------
            
            box { <-1.00, -1.00, .200>,< 1.00, 2.00, 1.00>   
            texture { HautFarbe } // end of texture
            scale <1,1,1> rotate<0,0,0> translate<0,0,0> 
            } // end of box --------------------------------------                        
            translate<0,-1,0>
         }//Ende von Difference
         translate<0,-2,0>
    }                                                           

#declare KopfHoehe=2;
#declare Kopf =
    union
    {  
        
       cylinder { <0,0,0>,<0,4.00,0>, 0.9

           texture {HautFarbe} // end of texture

           scale <1,1,1> rotate<0,0,0> translate<0,0,0>
         } // end of cylinder -------------------------------------        
         
       cylinder { <0,0.5,0>,<0,0.6,0>, 0.5

                            texture { pigment{ color rgb<1.0, 0.5, 0.75> } // pale red violet
               // normal { bumps 0.5 scale 0.05 }
                  finish { phong 1 reflection 0.00}
                } // end of texture 
                
           scale <1.2,1,1> rotate<0,0,0> translate<0,0,-0.5>
         } // end of cylinder -------------------------------------   
         
         
        object{Auge
                scale 0.5 translate <-0.4,KopfHoehe*2*5/12,-0.9>}
        object{Auge
                scale 0.5 translate <0.4,KopfHoehe*2*5/12,-0.9>}                
                
        object{Nase
                scale 0.5 translate <0,KopfHoehe*2*2/7,-0.8>}                 
        scale 0.5        
    } //Ende union
                                                 

/*
object{Nase}        */    
union
{
object{Kopf translate<0,BeinHoehe+OberkoerperHoehe,0> }
object{Zylinder translate<0,-ZylinderHoehe/2+BeinHoehe+OberkoerperHoehe+KopfHoehe+0.1,0>}
/**/object{Oberkoerper translate<0,BeinHoehe,0>}  

object{Arm translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0>}
object{Arm translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0>}

object{Bein translate<UnterkoerperBreite/2-0.4,BeinHoehe,0>}
object{Bein translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0>}
translate<0,0,0>}
                                 
