// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc" 

//#include "Katapult.pov"
#include "Typ.pov"
#include "Wald.pov"
#include "Berg_gross.pov"

//--------------------------------------------------------------------------
// camera ------------------------------------------------------------------
#declare Camera_0 = camera {angle 75      // front view
                            location  <0.0 , 1.0 ,-2.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_1 = camera { angle 90   // diagonal view
                            location  <2.0 , 2.5 ,-3.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_2 = camera {angle 90 // right side view
                            location  <1.0 , 1.0 , -1.0>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , -1.0>}
#declare Camera_3 = camera { angle 90        // top view
                            location  <2.0 , 2.0 ,-2.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_4 = camera { angle 90        // top view
                            location  <10.0 , 5.0 ,-10.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}    
#declare Camera_5 = camera { angle 90        // top view
                            location  <20.0 , 10.0 ,-20.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 1.0 , 0.0>}
#declare Camera_6 = camera { angle 90        // top view
                            location  <-0.0 , 100.0 ,-100.001>
                            right     x*image_width/image_height
                            look_at   <0.0 , 10.0 , 0.0>}
#declare Camera_11 = camera { angle 90        // top view
                            location  <-150+5,(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5,197>
                            right     x*image_width/image_height
                            look_at  <-147-10,(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5+2,199>} 
                                                           
#if((clock>0.5)&(clock<=0.9))
#declare Camera_10 = camera { angle 90        // top view
                            location  <-148+2*sin((clock-0.5)/0.4*pi/2),(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5,197>
                            right     x*image_width/image_height
                            look_at  <-152-10*sin((clock-0.5)/0.4*pi/2),(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5+10*sin((clock-0.5)/0.4*pi/2),199>}  
#end                                                                                                                                                  
#if(clock>0.9)
#declare Camera_10 = camera { angle 90        // top view
                            location  <-146,(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5,197>
                            right     x*image_width/image_height
                            look_at  <-162,(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5+10,199>}                                                                                                                        
#end
#if(clock<=0.5)
#declare Camera_10 = camera { angle 90        // top view
                            location  <-150+2*sin(clock/0.5*pi/2),(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5,197>
                            right     x*image_width/image_height
                            look_at  <-147-5*sin(clock/0.5*pi/2),(0.5*(BeinHoehe+OberkoerperHoehe+KopfHoehe/2))-0.5,199>}                                                                                                                                                    
#end                            
camera{Camera_10}
// sun ---------------------------------------------------------------------
light_source{<-1500,2000,-2500> color White}

// sky -------------------------------------------------------------- 
plane{<0,1,0>,1 hollow  
       texture{ pigment{ bozo turbulence 0.92
                         color_map { [0.00 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.50 rgb <0.20, 0.20, 1.0>*0.9]
                                     [0.70 rgb <1,1,1>]
                                     [0.85 rgb <0.25,0.25,0.25>]
                                     [1.0 rgb <0.5,0.5,0.5>]}
                        scale<1,1,1.5>*2.5  translate< 0,0,0>
                       }
                finish {ambient 1 diffuse 0} }      
       scale 10000}
// fog on the ground -------------------------------------------------
fog { fog_type   2
      distance   50
      color      White  
      fog_offset 0.1
      fog_alt    1.5
      turbulence 1.8
    }

// ground ------------------------------------------------------------
difference{
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.72 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane
     
}//Ende von Difference
//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//--------------------------------------------------------------------------  
       
      


          
#local Typfaktor = 0.5;

//Typ                                                  
union{
object{Kopf rotate<sin(clock*pi/2)*20,0,0> translate<0,BeinHoehe+OberkoerperHoehe,0> scale Typfaktor }
object{Zylinder translate<0,-ZylinderHoehe/2+KopfHoehe,0> rotate<20*sin(clock*pi/2),0,0> translate<0,BeinHoehe+OberkoerperHoehe+0.1,0> scale Typfaktor }
object{Oberkoerper translate<0,BeinHoehe,0> scale Typfaktor }  

#if(clock<=0.8)
    object{Arm rotate <0,0,-80> rotate<-20*sin(clock*2*pi),0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }
    object{Arm rotate <0,0,-80> rotate<-20*sin(clock*2*pi),0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor }
#else
    object{Arm rotate <0,30*sin((clock-0.8)/0.2*pi/2),-80+150*sin((clock-0.8)/0.2*pi/2)> rotate<0,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> scale Typfaktor }
    object{Arm rotate <0,-40*sin((clock-0.8)/0.2*pi/2),-80+140*sin((clock-0.8)/0.2*pi/2)> rotate<0,0,0> translate<OberkoerperBreite/2,ArmHoehe+BeinHoehe,0> rotate<0,180,0> scale Typfaktor }
#end    

object{Bein rotate<30*sin(clock*2*pi),0,0> translate<UnterkoerperBreite/2-0.4,BeinHoehe,0> scale Typfaktor }
object{Bein rotate<-30*sin(clock*2*pi),0,0> translate<-UnterkoerperBreite/2+0.4,BeinHoehe,0> scale Typfaktor }
//rotate<0,90,0> translate<-150,0,199>
#if(clock<0.8)
    rotate<0,90,0> translate<-147-4*sin(clock/0.8*pi/2),0,199>
#else
    rotate<0,90,0> translate<-147-4,sin((clock-0.8)/0.2*pi/2)*0.2,199>
#end
}

              

//Umgebung                                                      
union
{
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-130,0,0> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<-40,0,0> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<50,0,0> rotate<0,90,0>}
    object{Wald translate<0,0,0> rotate <0,90,0> scale 3 translate<140,0,0> rotate<0,90,0>}
    translate<-50,0,250>
}    

object{Berg scale 3 rotate<0,-90,0> translate<0,-10,0> scale <2,3,4> translate<-180,0,-100>}       






   









